import path from 'path';
import express from 'express';
import { GraphQLServer } from 'graphql-yoga';
import resolvers from './resolvers';
import createContext from './helpers/context.helper';
import env from './constants/env.constants';

const server = new GraphQLServer({
  typeDefs: path.join(__dirname, 'schemas', 'api.graphql'),
  resolvers,
  context: createContext,
});

server.express.use('/avatars', express.static(env.uploads.avatarsPath));

server.express.use(
  '/product-pictures',
  express.static(env.uploads.productPicturesPath),
);

export default server;
