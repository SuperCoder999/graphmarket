import { PrismaClient, User } from '@prisma/client';
import { Request, Response } from 'express';

export interface Context {
  prisma: PrismaClient;
  request: Request;
  response: Response;
  user: User | null;
}

export interface Resolver<Result = any, Args = any> {
  (parent: any, args: Args, context: Context, info: any):
    | Result
    | Promise<Result>;
}

export interface ResolversMap<Result = any, Args = any> {
  [name: string]: Resolver<Result, Args> | ResolversMap;
}
