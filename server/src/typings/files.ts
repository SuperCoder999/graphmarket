export interface GraphQLFile {
  filename: string;
  base64: string;
}
