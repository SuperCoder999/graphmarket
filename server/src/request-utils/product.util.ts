const getProductRelationsSelect = (userId: string) => ({
  include: {
    seller: {
      include: {
        avatar: true,
      },
    },
    pictures: {
      select: {
        id: true,
        url: true,
      },
    },
    cartItems: {
      select: {
        id: true,
      },
      where: {
        userId,
      },
    },
  },
});

export default getProductRelationsSelect;
