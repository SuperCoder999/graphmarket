import HttpException from './http.exception';

export default class NotUniqueEmailPhoneException extends HttpException {
  public constructor() {
    super('Email or phone number is already taken', 422);
  }
}
