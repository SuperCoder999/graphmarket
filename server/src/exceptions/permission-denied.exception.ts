import HttpException from './http.exception';

export default class PermissionDeniedException extends HttpException {
  public constructor() {
    super('Permission denied', 403);
  }
}
