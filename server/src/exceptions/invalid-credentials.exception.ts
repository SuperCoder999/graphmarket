import HttpException from './http.exception';

export default class InvalidCredentialsException extends HttpException {
  public constructor() {
    super('Invalid credentials', 401);
  }
}
