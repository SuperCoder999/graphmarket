import HttpException from './http.exception';

export default class UnauthorizedException extends HttpException {
  public constructor() {
    super('Unauthorized', 401);
  }
}
