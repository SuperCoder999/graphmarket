import HttpException from './http.exception';

export default class NotInCartException extends HttpException {
  public constructor() {
    super('Not in cart', 404);
  }
}
