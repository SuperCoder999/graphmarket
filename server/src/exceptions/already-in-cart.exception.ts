import HttpException from './http.exception';

export default class AlreadyInCartException extends HttpException {
  public constructor() {
    super('Already in cart', 409);
  }
}
