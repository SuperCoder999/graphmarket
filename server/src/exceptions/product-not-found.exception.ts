import HttpException from './http.exception';

export default class ProductNotFound extends HttpException {
  public constructor() {
    super('Product not found', 404);
  }
}
