import path from 'path';
import jwt from 'jsonwebtoken';

const { PORT, BASE_URL, JWT_SECRET } = process.env;

const env = {
  application: {
    port: PORT ? Number(PORT) : 3000,
    baseUrl: BASE_URL ?? 'http://localhost3000',
  },
  authentication: {
    secret: JWT_SECRET ?? '',
    algorithm: 'HS512' as jwt.Algorithm,
    prefix: 'Bearer ',
  },
  uploads: {
    avatarsPath: path.join(__dirname, '..', '..', 'uploads', 'avatars'),
    productPicturesPath: path.join(
      __dirname,
      '..',
      '..',
      'uploads',
      'product-pictures',
    ),
  },
};

export default env;
