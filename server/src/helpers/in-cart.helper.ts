import { Product } from '@prisma/client';

type ProductCartItems = Product & { cartItems: { id: string }[] };
type ProductIsInCart = Product & { isInCart: boolean };

export default function addIsInCart(data: ProductCartItems): ProductIsInCart;
export default function addIsInCart(
  data: ProductCartItems[],
): ProductIsInCart[];
export default function addIsInCart(
  data: ProductCartItems | ProductCartItems[],
): ProductIsInCart | ProductIsInCart[] {
  if (Array.isArray(data)) {
    return data.map((product) => ({
      ...product,
      isInCart: product.cartItems.length > 0,
    }));
  }

  return {
    ...data,
    isInCart: data.cartItems.length > 0,
  };
}
