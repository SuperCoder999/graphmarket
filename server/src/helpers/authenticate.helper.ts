import { User } from '@prisma/client';
import { Request } from 'express';
import env from '../constants/env.constants';
import UnauthorizedException from '../exceptions/unauthorized.exception';
import { decodeUserToken } from './jwt.helper';
import prisma from './prisma.helper';

export async function authenticate(request: Request): Promise<User | null> {
  const header = request.headers.authorization;

  if (!header || !header.startsWith(env.authentication.prefix)) {
    return null;
  }

  const id = decodeUserToken(
    header.substring(env.authentication.prefix.length),
  );

  return prisma.user.findFirst({ where: { id }, include: { avatar: true } });
}

export function authReqired(user?: User | null) {
  if (!user) {
    throw new UnauthorizedException();
  }
}
