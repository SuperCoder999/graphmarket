import path from 'path';
import dotenv from 'dotenv';

export default function initEnv() {
  dotenv.config({
    path: path.join(__dirname, '..', '..', '.env'),
  });
}
