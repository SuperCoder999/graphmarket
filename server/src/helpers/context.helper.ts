import { Context } from '../typings/resolvers';
import { authenticate } from './authenticate.helper';
import prisma from './prisma.helper';

export default async function createContext(
  context: Context,
): Promise<Context> {
  return {
    ...context,
    prisma,
    user: await authenticate(context.request),
  };
}
