import jwt from 'jsonwebtoken';
import env from '../constants/env.constants';

export function createUserToken(id: string): string {
  return jwt.sign({ id }, env.authentication.secret, {
    algorithm: env.authentication.algorithm,
  });
}

export function decodeUserToken(token: string): string {
  const payload = jwt.verify(token, env.authentication.secret, {
    algorithms: [env.authentication.algorithm],
  });

  return (payload as { id: string }).id;
}
