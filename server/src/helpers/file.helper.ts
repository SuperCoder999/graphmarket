import fs from 'fs';
import path from 'path';
import env from '../constants/env.constants';
import { GraphQLFile } from '../typings/files';

function getName(existingName: string): string {
  const existingNameParts = existingName.split('.');

  const extension =
    existingNameParts.length > 1 ? `.${existingNameParts[1]}` : '';

  const prefixAlphabet =
    'abcdefghijklmnopqrstuvqxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

  let prefix: string = '';

  for (let i = 0; i < 16; i++) {
    const random: number = Math.random();
    const randomIndex = Math.floor(random * prefixAlphabet.length);
    prefix += prefixAlphabet[randomIndex];
  }

  return `${prefix}${extension}`;
}

async function uploadFile(
  file: GraphQLFile,
  uploadPath: string,
  fileGroup: 'avatars' | 'product-pictures',
): Promise<string> {
  const fileName = getName(file.filename);
  const filePermanentPath = path.join(uploadPath, fileName);

  await fs.promises.mkdir(uploadPath, { recursive: true });
  const text: Buffer = Buffer.from(file.base64, 'base64');

  const writeStream = fs.createWriteStream(filePermanentPath);
  writeStream.end(text);

  const url = `${env.application.baseUrl}/${fileGroup}/${fileName}`;
  return url;
}

export function uploadAvatar(file: GraphQLFile): Promise<string> {
  return uploadFile(file, env.uploads.avatarsPath, 'avatars');
}

export function uploadProductImage(file: GraphQLFile): Promise<string> {
  return uploadFile(file, env.uploads.productPicturesPath, 'product-pictures');
}
