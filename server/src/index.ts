import initEnv from './helpers/env.helper';
initEnv();

import env from './constants/env.constants';
import server from './server';

server.start(
  { port: env.application.port, bodyParserOptions: { limit: '20mb' } },
  () => console.log(`Server is running on port ${env.application.port}`),
);
