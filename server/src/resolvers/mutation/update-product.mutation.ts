import { Product } from '@prisma/client';
import PermissionDeniedException from '../../exceptions/permission-denied.exception';
import ProductNotFound from '../../exceptions/product-not-found.exception';
import { authReqired } from '../../helpers/authenticate.helper';
import { uploadProductImage } from '../../helpers/file.helper';
import addIsInCart from '../../helpers/in-cart.helper';
import getProductRelationsSelect from '../../request-utils/product.util';
import { GraphQLFile } from '../../typings/files';
import { Resolver } from '../../typings/resolvers';

const updateProduct: Resolver<
  Product,
  Partial<Product> & { pictures?: GraphQLFile[]; deletedPictures?: string[] }
> = async (
  _,
  { id, pictures: pictureFiles, deletedPictures, ...data },
  { prisma, user },
) => {
  authReqired(user);

  const product = await prisma.product.findFirst({
    where: { id },
    select: { sellerId: true },
  });

  if (!product) {
    throw new ProductNotFound();
  }

  if (product.sellerId !== user?.id) {
    throw new PermissionDeniedException();
  }

  const pictures =
    pictureFiles && pictureFiles.length > 0
      ? await Promise.all(
          pictureFiles.map(async (file) => {
            const url = await uploadProductImage(file);
            return { url };
          }),
        )
      : null;

  const createPicturesModifier = pictures
    ? {
        createMany: {
          data: pictures,
        },
      }
    : {};

  const deletedPicturesModifier =
    deletedPictures && deletedPictures.length > 0
      ? {
          deleteMany: deletedPictures.map((id) => ({
            id,
          })),
        }
      : {};

  const result = await prisma.product.update({
    where: { id },
    data: {
      ...data,
      pictures: {
        ...createPicturesModifier,
        ...deletedPicturesModifier,
      },
    },
    ...getProductRelationsSelect(user!.id),
  });

  return addIsInCart(result);
};

export default updateProduct;
