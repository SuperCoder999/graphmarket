import NotInCartException from '../../exceptions/not-in-cart.expection';
import { authReqired } from '../../helpers/authenticate.helper';
import { Resolver } from '../../typings/resolvers';

const removeFromCart: Resolver<string, { id: string }> = async (
  _,
  { id },
  { prisma, user },
) => {
  authReqired(user);

  const item = await prisma.cartItem.findFirst({
    where: { userId: user?.id, productId: id },
    select: { id: true },
  });

  if (!item) {
    throw new NotInCartException();
  }

  await prisma.cartItem.delete({
    where: { id: item.id },
  });

  return id;
};

export default removeFromCart;
