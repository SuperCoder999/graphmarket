import { Product } from '@prisma/client';
import AlreadyInCartException from '../../exceptions/already-in-cart.exception';
import ProductNotFound from '../../exceptions/product-not-found.exception';
import { authReqired } from '../../helpers/authenticate.helper';
import getProductRelationsSelect from '../../request-utils/product.util';
import { Resolver } from '../../typings/resolvers';

const addToCart: Resolver<Product, { id: string }> = async (
  _,
  { id },
  { prisma, user },
) => {
  authReqired(user);

  const product = await prisma.product.findFirst({
    where: { id },
    ...getProductRelationsSelect(user!.id),
  });

  if (!product) {
    throw new ProductNotFound();
  }

  const data = {
    userId: user!.id,
    productId: product.id,
  };

  const existing = await prisma.cartItem.findFirst({
    where: data,
  });

  if (existing) {
    throw new AlreadyInCartException();
  }

  await prisma.cartItem.create({ data });

  return {
    ...product,
    isInCart: product.cartItems.length > 0,
  };
};

export default addToCart;
