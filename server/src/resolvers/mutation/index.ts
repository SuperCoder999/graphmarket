import { ResolversMap } from '../../typings/resolvers';
import login from './login.mutation';
import register from './register.mutation';
import createProduct from './create-product.mutation';
import updateProduct from './update-product.mutation';
import deleteProduct from './delete-product.mutation';
import addToCart from './add-to-cart.mutation';
import removeFromCart from './remove-from-cart.mutation';

const mutation: ResolversMap = {
  login,
  register,
  createProduct,
  updateProduct,
  deleteProduct,
  addToCart,
  removeFromCart,
};

export default mutation;
