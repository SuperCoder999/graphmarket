import { User } from '@prisma/client';
import { Resolver } from '../../typings/resolvers';
import InvalidCredentialsException from '../../exceptions/invalid-credentials.exception';
import { checkPassword } from '../../helpers/password.helper';
import { createUserToken } from '../../helpers/jwt.helper';

const login: Resolver<
  User & { token: string },
  {
    email: string;
    password: string;
  }
> = async (_, { email, password }, { prisma }) => {
  const user = await prisma.user.findFirst({
    where: { email },
    include: { avatar: true },
  });

  if (!user) {
    throw new InvalidCredentialsException();
  }

  const validPassword: boolean = await checkPassword(password, user.password);

  if (!validPassword) {
    throw new InvalidCredentialsException();
  }

  return {
    ...user,
    token: createUserToken(user.id),
  };
};

export default login;
