import PermissionDeniedException from '../../exceptions/permission-denied.exception';
import ProductNotFound from '../../exceptions/product-not-found.exception';
import { authReqired } from '../../helpers/authenticate.helper';
import { Resolver } from '../../typings/resolvers';

const deleteProduct: Resolver<string, { id: string }> = async (
  _,
  { id },
  { prisma, user },
) => {
  authReqired(user);

  const product = await prisma.product.findFirst({
    where: { id },
    select: { sellerId: true },
  });

  if (!product) {
    throw new ProductNotFound();
  }

  if (product.sellerId !== user?.id) {
    throw new PermissionDeniedException();
  }

  const deleted = await prisma.product.delete({
    where: { id },
  });

  return deleted.id;
};

export default deleteProduct;
