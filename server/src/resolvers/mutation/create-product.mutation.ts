import { Product } from '@prisma/client';
import { authReqired } from '../../helpers/authenticate.helper';
import { uploadProductImage } from '../../helpers/file.helper';
import addIsInCart from '../../helpers/in-cart.helper';
import getProductRelationsSelect from '../../request-utils/product.util';
import { GraphQLFile } from '../../typings/files';
import { Resolver } from '../../typings/resolvers';

const createProduct: Resolver<
  Product,
  Omit<Product, 'id'> & { pictures?: GraphQLFile[] }
> = async (_, { pictures: pictureFiles, ...data }, { prisma, user }) => {
  authReqired(user);

  const pictures =
    pictureFiles && pictureFiles.length > 0
      ? await Promise.all(
          pictureFiles.map(async (file) => {
            const url = await uploadProductImage(file);
            return { url };
          }),
        )
      : null;

  const result = await prisma.product.create({
    data: {
      ...data,
      sellerId: user!.id,
      pictures: {
        ...(pictures
          ? {
              createMany: {
                data: pictures,
              },
            }
          : {}),
      },
    },
    ...getProductRelationsSelect(user!.id),
  });

  return addIsInCart(result);
};

export default createProduct;
