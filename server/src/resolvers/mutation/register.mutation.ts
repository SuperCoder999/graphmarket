import { User } from '@prisma/client';
import { Resolver } from '../../typings/resolvers';
import { hashPassword } from '../../helpers/password.helper';
import { createUserToken } from '../../helpers/jwt.helper';
import { GraphQLFile } from '../../typings/files';
import { uploadAvatar } from '../../helpers/file.helper';
import NotUniqueEmailPhoneException from '../../exceptions/not-unique-email-phone.exception';

const register: Resolver<
  User & { token: string },
  User & { avatar?: GraphQLFile }
> = async (_, { avatar, ...data }, { prisma }) => {
  const avatarUrl = avatar ? await uploadAvatar(avatar) : null;

  let user: User;

  try {
    user = await prisma.user.create({
      data: {
        ...data,
        password: await hashPassword(data.password),
        avatarId: undefined,
        avatar: avatarUrl
          ? {
              create: { url: avatarUrl },
            }
          : undefined,
      },
      include: { avatar: true },
    });
  } catch {
    throw new NotUniqueEmailPhoneException();
  }

  return {
    ...user,
    token: createUserToken(user.id),
  };
};

export default register;
