import { ResolversMap } from '../typings/resolvers';
import query from './query';
import mutation from './mutation';

const resolvers: ResolversMap = {
  Query: query,
  Mutation: mutation,
};

export default resolvers;
