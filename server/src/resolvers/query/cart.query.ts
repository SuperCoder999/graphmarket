import { Product } from '@prisma/client';
import { authReqired } from '../../helpers/authenticate.helper';
import addIsInCart from '../../helpers/in-cart.helper';
import getProductRelationsSelect from '../../request-utils/product.util';
import { Resolver } from '../../typings/resolvers';

const cart: Resolver<Product[]> = async (_parent, _args, { prisma, user }) => {
  authReqired(user);

  const products = await prisma.product.findMany({
    where: {
      cartItems: {
        some: {
          userId: user!.id,
        },
      },
    },
    ...getProductRelationsSelect(user!.id),
  });

  return addIsInCart(products);
};

export default cart;
