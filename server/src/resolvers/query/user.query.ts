import { User } from '@prisma/client';
import { Resolver } from '../../typings/resolvers';

const user: Resolver<User | null> = (_parent, _args, { user }) => {
  return user;
};

export default user;
