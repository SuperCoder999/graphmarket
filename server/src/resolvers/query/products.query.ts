import { Product } from '@prisma/client';
import { authReqired } from '../../helpers/authenticate.helper';
import addIsInCart from '../../helpers/in-cart.helper';
import getProductRelationsSelect from '../../request-utils/product.util';
import { Resolver } from '../../typings/resolvers';

const products: Resolver<
  { products: Product[]; hasMoreProducts: boolean },
  { page?: number; perPage?: number; search?: number }
> = async (_, { page, perPage, search }, { prisma, user }) => {
  authReqired(user);

  let pagination = {};
  let where = {};
  let hasMoreProducts = false;

  if (page && perPage) {
    const skip = (page - 1) * perPage;

    pagination = {
      skip,
      take: perPage,
    };

    const count = await prisma.product.count({ where });
    const maxSelectedCount = perPage * page;

    hasMoreProducts = count > maxSelectedCount;
  }

  if (search) {
    where = {
      title: {
        contains: search,
      },
    };
  }

  const result = await prisma.product.findMany({
    ...getProductRelationsSelect(user!.id),
    ...pagination,
    where,
  });

  return {
    products: addIsInCart(result),
    hasMoreProducts,
  };
};

export default products;
