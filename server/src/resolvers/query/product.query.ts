import { Product } from '@prisma/client';
import { authReqired } from '../../helpers/authenticate.helper';
import addIsInCart from '../../helpers/in-cart.helper';
import getProductRelationsSelect from '../../request-utils/product.util';
import { Resolver } from '../../typings/resolvers';

const product: Resolver<Product | null, { id: string }> = async (
  _,
  { id },
  { prisma, user },
) => {
  authReqired(user);

  const result = await prisma.product.findFirst({
    where: { id },
    ...getProductRelationsSelect(user!.id),
  });

  return result ? addIsInCart(result) : null;
};

export default product;
