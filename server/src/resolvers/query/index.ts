import { ResolversMap } from '../../typings/resolvers';
import user from './user.query';
import products from './products.query';
import product from './product.query';
import cart from './cart.query';

const query: ResolversMap = {
  user,
  products,
  product,
  cart,
};

export default query;
