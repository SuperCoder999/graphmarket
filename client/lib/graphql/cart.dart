// ignore: implementation_imports
import 'package:gql/src/ast/ast.dart';
import 'package:graphql/client.dart';

class CartQueries {
  static DocumentNode readCart = gql('''
    query {
      cart {
        id
        title
        price
        description
        sellerId
        isInCart
        pictures {
          id
          url
        }
      }
    }
  ''');

  static DocumentNode addToCart = gql('''
    mutation (\$id: ID!) {
      addToCart(id: \$id) {
        id
        title
        price
        description
        isInCart
        seller {
          id
          name
          phoneNumber
          avatar {
            id
            url
          }
        }
        pictures {
          id
          url
        }
      }
    }
  ''');

  static DocumentNode removeFromCart = gql('''
    mutation (\$id: ID!) {
      removeFromCart(id: \$id)
    }
  ''');
}
