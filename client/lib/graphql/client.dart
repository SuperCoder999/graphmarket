import 'package:graphql/client.dart';
import 'package:marketplace/constants/http-settings.dart';
import 'package:marketplace/helpers/exceptions.dart';
import 'package:marketplace/helpers/storage.dart';

class _BearerStore {
  static Future<String?> get() async {
    String? token = await Storage.get(StorageKey.userToken);

    if (token == null) {
      return null;
    }

    return 'Bearer $token';
  }
}

class MarketplaceGraphQLClient extends GraphQLClient {
  MarketplaceGraphQLClient() : super(
    link: AuthLink(getToken: () => _BearerStore.get())
      .concat(HttpLink(HttpSettings.apiBaseUrl)),
    cache: GraphQLCache(store: InMemoryStore()),
    defaultPolicies: DefaultPolicies(
      query: Policies(fetch: FetchPolicy.noCache),
      mutate: Policies(fetch: FetchPolicy.noCache),
    ),
  );
}

class MarketplaceGraphQL {
  final MarketplaceGraphQLClient client = MarketplaceGraphQLClient();

  Map<String, dynamic>? _processResult(QueryResult result) {
    if (result.hasException) {
      ExceptionsHelper.handleGraphQLError(result.exception);
      return null;
    }

    return result.data;
  }

  Future<Map<String, dynamic>?> query(QueryOptions options) async {
    QueryResult result = await client.query(options);
    return _processResult(result);
  }

  Future<Map<String, dynamic>?> mutate(MutationOptions options) async {
    QueryResult result = await client.mutate(options);
    return _processResult(result);
  }
}

final MarketplaceGraphQL graphQL = MarketplaceGraphQL();
