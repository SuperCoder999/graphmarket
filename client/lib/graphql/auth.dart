// ignore: implementation_imports
import 'package:gql/src/ast/ast.dart';
import 'package:graphql/client.dart';

class AuthQueries {
  static DocumentNode login = gql('''
    mutation (\$email: String!, \$password: String!) {
      login(email: \$email, password: \$password) {
        id
        token
      }
    }
  ''');

  static DocumentNode register = gql('''
    mutation (\$name: String!, \$email: String!, \$phoneNumber: String!, \$password: String!, \$avatar: File) {
      register(name: \$name, email: \$email, phoneNumber: \$phoneNumber, password: \$password, avatar: \$avatar) {
        id
        token
      }
    }
  ''');

  static DocumentNode getUser = gql('''
    query {
      user {
        id
      }
    }
  ''');
}
