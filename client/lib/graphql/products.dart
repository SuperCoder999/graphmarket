// ignore: implementation_imports
import 'package:gql/src/ast/ast.dart';
import 'package:graphql/client.dart';
import 'package:marketplace/constants/app-settings.dart';

class ProductQueries {
  static DocumentNode getAll = gql('''
    query (\$page: Int, \$search: String) {
      products(page: \$page, perPage: ${Settings.productsPerPage}, search: \$search) {
        products {
          id
          title
          price
          description
          sellerId
          isInCart
          pictures {
            id
            url
          }
        }
        hasMoreProducts
      }
    }
  ''');

  static DocumentNode getExpanded = gql('''
    query (\$id: ID!) {
      product(id: \$id) {
        id
        title
        price
        description
        isInCart
        seller {
          id
          name
          phoneNumber
          avatar {
            id
            url
          }
        }
        pictures {
          id
          url
        }
      }
    }
  ''');

  static DocumentNode create = gql('''
    mutation (\$title: String!, \$price: Float!, \$description: String, \$pictures: [File!]) {
      createProduct(title: \$title, price: \$price, description: \$description, pictures: \$pictures) {
        id
        title
        price
        description
        isInCart
        seller {
          id
          name
          phoneNumber
          avatar {
            id
            url
          }
        }
        pictures {
          id
          url
        }
      }
    }
  ''');

  static DocumentNode update = gql('''
    mutation (\$id: ID!, \$title: String, \$price: Float, \$description: String, \$pictures: [File!], \$deletedPictures: [ID!]) {
      updateProduct(id: \$id, title: \$title, price: \$price, description: \$description, pictures: \$pictures, deletedPictures: \$deletedPictures) {
        id
        title
        price
        description
        isInCart
        seller {
          id
          name
          phoneNumber
          avatar {
            id
            url
          }
        }
        pictures {
          id
          url
        }
      }
    }
  ''');

  static DocumentNode delete = gql('''
    mutation (\$id: ID!) {
      deleteProduct(id: \$id)
    }
  ''');
}
