class RequestException implements Exception {
  final String message;
  final int statusCode;

  RequestException({
    required this.message,
    required this.statusCode,
  });

  @override
  String toString() {
    return 'RequestException: $message';
  }
}
