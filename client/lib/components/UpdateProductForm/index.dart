import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:marketplace/components/ImagePicker/MultipleImagePicker/index.dart';
import 'package:marketplace/components/Spacer/InputSpacer/index.dart';
import 'package:marketplace/dtos/picture/picure.dart';
import 'package:marketplace/dtos/product/product.dart';
import 'package:marketplace/dtos/product/update-product.dart';
import 'package:marketplace/helpers/validation.dart';
import 'package:marketplace/redux/products/action-types.dart';
import 'package:marketplace/redux/store.dart';

typedef OnUpdateProductSubmit = void Function(UpdateProductDto);

class UpdateProductForm extends StatefulWidget {
  final OnUpdateProductSubmit onSubmit;
  final ProductDto existing;

  const UpdateProductForm({
    Key? key,
    required this.onSubmit,
    required this.existing,
  }) : super(key: key);

  @override
  _UpdateProductFormState createState() => _UpdateProductFormState();
}

class _UpdateProductFormState extends State<UpdateProductForm> {
  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  late UpdateProductDto _data;
  late List<PictureDto> _existingPictures;

  @override
  void initState() {
    super.initState();

    _data = UpdateProductDto(
      id: widget.existing.id,
      title: widget.existing.title,
      description: widget.existing.description,
      price: widget.existing.price,
      createdPictures: [],
      deletedPictures: [],
    );

    _existingPictures = widget.existing.pictures;
  }

  @override
  void dispose() {
    super.dispose();
    marketplaceStore.dispatch(RemoveUpdatingProduct());
  }

  void setTitle(String? value) {
    _data.title = value ?? '';
  }

  void setDescription(String? value) {
    _data.description = value ?? '';
  }

  void setPrice(double? value) {
    _data.price = value ?? 0;
  }

  void setPictures(List<XFile>? value) {
    _data.createdPictures = value ?? [];
  }

  void addDeletedPicture(String id) {
    setState(() {
      _existingPictures = _existingPictures
        .where((picture) => picture.id != id)
        .toList();
    });

    _data.deletedPictures.add(id);
  }

  void submit() {
    bool isValid = _form.currentState!.validate();

    if (!isValid) {
      return;
    }

    _form.currentState!.save();
    widget.onSubmit(_data);
  }

  @override
  Widget build(BuildContext context) {
    final TextEditingController titleInit = TextEditingController(text: widget.existing.title);
    final TextEditingController descriptionInit = TextEditingController(text: widget.existing.description);
    final TextEditingController priceInit = TextEditingController(text: widget.existing.price.toString());

    return Form(
      key: _form,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        children: [
          FormField<String>(
            builder: (field) {
              return TextField(
                decoration: InputDecoration(
                  label: const Text('Title'),
                  errorText: field.errorText,
                ),
                controller: titleInit,
                onChanged: field.didChange,
              );
            },
            initialValue: widget.existing.title,
            validator: Validation.exists,
            onSaved: setTitle,
          ),
          const InputSpacer(),
          FormField<String>(
            builder: (field) {
              return TextField(
                decoration: const InputDecoration(
                  label: Text('Description'),
                ),
                controller: descriptionInit,
                maxLines: null,
                onChanged: field.didChange,
              );
            },
            initialValue: widget.existing.description,
            onSaved: setDescription,
          ),
          const InputSpacer(),
          FormField<double>(
            builder: (field) {
              return TextField(
                decoration: InputDecoration(
                  label: const Text('Price'),
                  errorText: field.errorText,
                ),
                controller: priceInit,
                keyboardType: TextInputType.number,
                onChanged: (value) => field.didChange(double.tryParse(value)),
              );
            },
            initialValue: widget.existing.price,
            validator: Validation.positive,
            onSaved: setPrice,
          ),
          const InputSpacer(),
          FormField<List<XFile>>(
            builder: (field) {
              return MultipleImagePicker(
                onPicked: field.didChange,
                existing: _existingPictures,
                onExistingDeleted: addDeletedPicture,
              );
            },
            onSaved: setPictures,
          ),
          const InputSpacer(),
          ElevatedButton(
            onPressed: submit,
            child: const Text('Update'),
          ),
        ],
      ),
    );
  }
}
