import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:marketplace/components/OptionalImage/index.dart';
import 'package:marketplace/components/Spacer/TinySpacer/index.dart';
import 'package:marketplace/dtos/product/product-list.dart';
import 'package:marketplace/dtos/user/user.dart';
import 'package:marketplace/helpers/text.dart';
import 'package:marketplace/redux/state.dart';

class ListProduct extends StatelessWidget {
  final ProductListDto product;
  final VoidCallback onShowDetails;
  final VoidCallback? onCartButtonClick;

  const ListProduct({
    Key? key,
    required this.product,
    required this.onShowDetails,
    this.onCartButtonClick,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String limitedDescription = TextHelper.limitWords(product.description);

    return StoreConnector<MarketplaceState, UserDto?>(
      converter: (store) => store.state.auth.user,
      builder: (context, user) {
        return Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.black87,
              width: 2,
            ),
          ),
          padding: const EdgeInsets.all(12),
          child: Row(
            children: [
              OptionalImage(link: product.picture?.url),
              const TinySpacer(horizontal: true),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      product.title,
                      style: const TextStyle(fontWeight: FontWeight.w500),
                    ),
                    const TinySpacer(),
                    Text(
                      product.price.toString() + '\$',
                      style: const TextStyle(fontWeight: FontWeight.w500),
                    ),
                    const TinySpacer(),
                    Text(limitedDescription),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        TextButton(
                          onPressed: onShowDetails,
                          child: Text(product.sellerId == user!.id ? 'Edit' : 'Details'),
                        ),
                        if (onCartButtonClick != null) TextButton(
                          onPressed: onCartButtonClick,
                          child: Text(product.isInCart ? 'Remove from cart' : 'Add to cart'),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
