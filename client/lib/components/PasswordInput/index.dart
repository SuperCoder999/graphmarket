import 'package:flutter/material.dart';

class PasswordInput extends StatefulWidget {
  final Widget? label;
  final String? errorText;
  final ValueChanged<String>? onChanged;

  const PasswordInput({
    Key? key,
    this.label,
    this.errorText,
    this.onChanged,
  }) : super(key: key);

  @override 
  _PasswordInputState createState() => _PasswordInputState();
}

class _PasswordInputState extends State<PasswordInput> {
  bool _isSecure = true;

  void toggleSecure() {
    setState(() {
      _isSecure = !_isSecure;
    });
  }

  @override
  Widget build(BuildContext context) {
    final IconData icon = _isSecure ? Icons.lock_open : Icons.lock_outline;

    return TextField(
      obscureText: _isSecure,
      onChanged: widget.onChanged,
      decoration: InputDecoration(
        label: widget.label,
        errorText: widget.errorText,
        suffixIcon: IconButton(
          icon: Icon(icon),
          splashRadius: 20,
          onPressed: toggleSecure,
        ),
      ),
    );
  }
}
