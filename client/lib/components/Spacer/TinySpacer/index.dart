import 'package:flutter/widgets.dart';

class TinySpacer extends StatelessWidget {
  static const double _size = 5;
  final bool horizontal;
  const TinySpacer({ Key? key, this.horizontal = false }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (horizontal) {
      return const SizedBox(width: _size);
    }

    return const SizedBox(height: _size);
  }
}
