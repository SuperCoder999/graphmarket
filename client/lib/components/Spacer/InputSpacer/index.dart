import 'package:flutter/widgets.dart';

class InputSpacer extends StatelessWidget {
  const InputSpacer({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(height: 15);
  }
}
