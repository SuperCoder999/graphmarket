import 'package:flutter/widgets.dart';

class FormSpacer extends StatelessWidget {
  const FormSpacer({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(height: 40);
  }
}
