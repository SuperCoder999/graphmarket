import 'package:flutter/material.dart';

class Spinner extends StatefulWidget {
  const Spinner({ Key? key }) : super(key: key);

  @override
  _SpinnerState createState() => _SpinnerState();
}

class _SpinnerState extends State<Spinner> {
  OverlayEntry? _overlay;

  @override
  void dispose() {
    super.dispose();

    _overlay?.remove();
    _overlay = null;
  }

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((_) {
      if (mounted) {
        _overlay = _createOverlay();
        Overlay.of(context)!.insert(_overlay!);
      }
    });
  }

  OverlayEntry _createOverlay() {
    return OverlayEntry(
      builder: (context) {
        final MediaQueryData media = MediaQuery.of(context);
        final double width = media.size.width * 0.8;

        return Positioned(
          top: 0,
          left: 0,
          height: media.size.height,
          child: Container(
            width: media.size.width,
            decoration: const BoxDecoration(color: Color(0x88ffffff)),
            child: Center(
              child: SizedBox(
                width: width,
                child: const LinearProgressIndicator(),
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    if (!mounted && _overlay != null) {
      _overlay!.remove();
      _overlay = null;
    }

    return Container();
  }
}
