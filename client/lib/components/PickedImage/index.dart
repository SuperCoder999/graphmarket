import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class PickedImage extends StatelessWidget {
  final XFile? image;
  final String? url;
  final VoidCallback onDeleted;
  static const double _imageSize = 50;
  static const double _iconSize = 18;

  const PickedImage({
    Key? key, 
    this.image,
    this.url,
    required this.onDeleted,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        if (image != null) Image.file(
          File(image!.path),
          width: _imageSize,
          height: _imageSize,
        )
        else if (url != null) Image.network(
          url!,
          width: _imageSize,
          height: _imageSize,
        ),
        Positioned(
          top: -_iconSize,
          right: -_iconSize,
          child: IconButton(
            icon: const Icon(Icons.delete),
            iconSize: _iconSize,
            splashRadius: 20,
            onPressed: onDeleted,
          ),
        ),
      ],
    );
  }
}
