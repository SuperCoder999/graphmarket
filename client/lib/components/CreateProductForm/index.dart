import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:marketplace/components/ImagePicker/MultipleImagePicker/index.dart';
import 'package:marketplace/components/Spacer/InputSpacer/index.dart';
import 'package:marketplace/dtos/product/create-product.dart';
import 'package:marketplace/helpers/validation.dart';

typedef OnCreateProductSubmit = void Function(CreateProductDto);

class CreateProductForm extends StatefulWidget {
  final OnCreateProductSubmit onSubmit;
  const CreateProductForm({ Key? key, required this.onSubmit }) : super(key: key);

  @override
  _CreateProductFormState createState() => _CreateProductFormState();
}

class _CreateProductFormState extends State<CreateProductForm> {
  final GlobalKey<FormState> _form = GlobalKey<FormState>();

  final CreateProductDto _data = CreateProductDto(
    title: '',
    description: '',
    price: 0,
    pictures: [],
  );

  void setTitle(String? value) {
    _data.title = value ?? '';
  }

  void setDescription(String? value) {
    _data.description = value ?? '';
  }

  void setPrice(double? value) {
    _data.price = value ?? 0;
  }

  void setPictures(List<XFile>? value) {
    _data.pictures = value ?? [];
  }

  void submit() {
    bool isValid = _form.currentState!.validate();

    if (!isValid) {
      return;
    }

    _form.currentState!.save();
    widget.onSubmit(_data);
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _form,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        children: [
          FormField<String>(
            builder: (field) {
              return TextField(
                decoration: InputDecoration(
                  label: const Text('Title'),
                  errorText: field.errorText,
                ),
                onChanged: field.didChange,
              );
            },
            validator: Validation.exists,
            onSaved: setTitle,
          ),
          const InputSpacer(),
          FormField<String>(
            builder: (field) {
              return TextField(
                decoration: const InputDecoration(
                  label: Text('Description'),
                ),
                maxLines: null,
                onChanged: field.didChange,
              );
            },
            onSaved: setDescription,
          ),
          const InputSpacer(),
          FormField<double>(
            builder: (field) {
              return TextField(
                decoration: InputDecoration(
                  label: const Text('Price'),
                  errorText: field.errorText,
                ),
                keyboardType: TextInputType.number,
                onChanged: (value) => field.didChange(double.tryParse(value)),
              );
            },
            validator: Validation.positive,
            onSaved: setPrice,
          ),
          const InputSpacer(),
          FormField<List<XFile>>(
            builder: (field) {
              return MultipleImagePicker(
                onPicked: field.didChange,
              );
            },
            onSaved: setPictures,
          ),
          const InputSpacer(),
          ElevatedButton(
            onPressed: submit,
            child: const Text('Create'),
          ),
        ],
      ),
    );
  }
}
