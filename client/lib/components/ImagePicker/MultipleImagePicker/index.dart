import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:marketplace/components/PickedImage/index.dart';
import 'package:marketplace/components/Spacer/TinySpacer/index.dart';
import 'package:marketplace/dtos/picture/picure.dart';

typedef OnImagesPick = void Function(List<XFile>);
typedef OnExistingImageDelete = void Function(String);

class MultipleImagePicker extends StatefulWidget {
  final ImagePicker picker = ImagePicker();
  final OnImagesPick onPicked;
  final OnExistingImageDelete? onExistingDeleted;
  final List<PictureDto>? existing;
  final String fieldName;

  MultipleImagePicker({
    Key? key,
    required this.onPicked,
    this.onExistingDeleted,
    this.existing,
    this.fieldName = 'images',
  }) : super(key: key);

  @override
  _MultipleImagePickerState createState() => _MultipleImagePickerState();
}

class _MultipleImagePickerState extends State<MultipleImagePicker> {
  List<XFile> _images = [];

  void pick() async {
    List<XFile>? files = await widget.picker.pickMultiImage();

    if (files != null) {
      List<XFile> newImages = [..._images, ...files];
      widget.onPicked(newImages);

      setState(() {
        _images = newImages;
      });
    }
  }

  void remove(int index) {
    List<XFile> newImages = [..._images];
    newImages.removeAt(index);
    widget.onPicked(newImages);

    setState(() {
      _images = newImages;
    });
  }

  void removeExisting(String id) {
    if (widget.onExistingDeleted != null) {
      widget.onExistingDeleted!(id);
    }
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> imageWidgets = [
      Wrap(
        children: [
          if (widget.existing != null) ...widget.existing!
            .map((image) => PickedImage(
              url: image.url,
              onDeleted: () => removeExisting(image.id)
            )),
          ..._images
            .asMap()
            .entries
            .map((image) => PickedImage(
              image: image.value,
              onDeleted: () => remove(image.key)
            )),
        ],
        spacing: 10,
        runSpacing: 10,
      ),
      const TinySpacer(),
    ];

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ...imageWidgets,
        ElevatedButton(
          onPressed: pick,
          child: Text('Pick ${widget.fieldName}'),
        ),
      ],
    );
  }
}
