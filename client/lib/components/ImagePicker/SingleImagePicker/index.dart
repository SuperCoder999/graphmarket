import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:marketplace/components/PickedImage/index.dart';
import 'package:marketplace/components/Spacer/TinySpacer/index.dart';

typedef OnImagePick = void Function(XFile?);

class SingleImagePicker extends StatefulWidget {
  final ImagePicker picker = ImagePicker();
  final OnImagePick onPicked;
  final String fieldName;

  SingleImagePicker({
    Key? key,
    required this.onPicked,
    this.fieldName = 'image',
  }) : super(key: key);

  @override
  _SingleImagePickerState createState() => _SingleImagePickerState();
}

class _SingleImagePickerState extends State<SingleImagePicker> {
  XFile? _image;

  void pick() async {
    XFile? file = await widget.picker.pickImage(source: ImageSource.gallery);

    if (file != null) {
      widget.onPicked(file);

      setState(() {
        _image = file;
      });
    }
  }

  void remove() {
    widget.onPicked(null);

    setState(() {
      _image = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> imageWidgets = _image == null ? const [] : [
      PickedImage(
        image: _image!,
        onDeleted: remove,
      ),
      const TinySpacer(),
    ];

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ...imageWidgets,
        ElevatedButton(
          onPressed: pick,
          child: Text('Pick ${widget.fieldName}'),
        ),
      ],
    );
  }
}
