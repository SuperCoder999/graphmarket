import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:marketplace/components/Spacer/FormSpacer/index.dart';
import 'package:marketplace/components/ImagePicker/SingleImagePicker/index.dart';
import 'package:marketplace/components/Spacer/InputSpacer/index.dart';
import 'package:marketplace/components/PasswordInput/index.dart';
import 'package:marketplace/dtos/user/user-register.dart';
import 'package:marketplace/helpers/validation.dart';

typedef OnRegisterSubmit = void Function(UserRegisterDto);

class RegisterForm extends StatefulWidget {
  final OnRegisterSubmit onSubmit;
  const RegisterForm({ Key? key, required this.onSubmit }) : super(key: key);

  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final UserRegisterDto _data = UserRegisterDto(
    email: '',
    name: '',
    phoneNumber: '',
    password: '',
  );

  final GlobalKey<FormState> _form = GlobalKey<FormState>();

  void setEmail(String? value) {
    _data.email = value ?? '';
  }

  void setName(String? value) {
    _data.name = value ?? '';
  }

  void setPhoneNumber(String? value) {
    _data.phoneNumber = value ?? '';
  }

  void setPassword(String? value) {
    _data.password = value ?? '';
  }

  void setAvatar(XFile? avatar) {
    _data.avatar = avatar;
  }

  void submit() {
    bool isValid = _form.currentState!.validate();

    if (!isValid) {
      return;
    }

    _form.currentState!.save();
    widget.onSubmit(_data);
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _form,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        children: [
          const FormSpacer(),
          FormField<String>(
            builder: (field) {
              return TextField(
                onChanged: (value) => field.didChange(value),
                decoration: InputDecoration(
                  label: const Text('Email'),
                  errorText: field.errorText,
                  suffixIcon: const Icon(Icons.email),
                ),
              );
            },
            onSaved: setEmail,
            validator: Validation.email,
          ),
          const InputSpacer(),
          FormField<String>(
            builder: (field) {
              return TextField(
                onChanged: (value) => field.didChange(value),
                decoration: InputDecoration(
                  label: const Text('Name'),
                  errorText: field.errorText,
                  suffixIcon: const Icon(Icons.person),
                ),
              );
            },
            onSaved: setName,
            validator: Validation.exists,
          ),
          const InputSpacer(),
          FormField<String>(
            builder: (field) {
              return TextField(
                keyboardType: TextInputType.phone,
                onChanged: (value) => field.didChange(value),
                decoration: InputDecoration(
                  label: const Text('Phone number'),
                  errorText: field.errorText,
                  suffixIcon: const Icon(Icons.phone),
                ),
              );
            },
            onSaved: setPhoneNumber,
            validator: Validation.phoneNumber,
          ),
          const InputSpacer(),
          FormField<String>(
            builder: (field) {
              return PasswordInput(
                onChanged: (value) => field.didChange(value),
                label: const Text('Password'),
                errorText: field.errorText
              );
            },
            onSaved: setPassword,
            validator: Validation.password,
          ),
          const InputSpacer(),
          FormField<XFile>(
            builder: (field) {
              return SingleImagePicker(
                onPicked: field.didChange,
                fieldName: 'avatar',
              );
            },
            onSaved: setAvatar,
          ),
          const FormSpacer(),
          ElevatedButton(
            onPressed: submit,
            child: const Text('Register'),
          ),
        ],
      ),
    );
  }
}
