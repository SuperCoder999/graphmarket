import 'package:flutter/material.dart';

class OptionalImage extends StatelessWidget {
  final double size;
  final String? link;

  const OptionalImage({
    Key? key,
    this.size = 70,
    this.link
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return link == null
      ? SizedBox(width: size, height: size)
      : Image.network(link!, width: size, height: size);
  }
}
