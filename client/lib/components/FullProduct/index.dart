import 'package:flutter/material.dart';
import 'package:marketplace/components/ImageCarousel/index.dart';
import 'package:marketplace/components/OptionalImage/index.dart';
import 'package:marketplace/components/Spacer/InputSpacer/index.dart';
import 'package:marketplace/components/Spacer/TinySpacer/index.dart';
import 'package:marketplace/dtos/product/product.dart';
import 'package:marketplace/dtos/user/user.dart';
import 'package:marketplace/helpers/phone.dart';

class FullProduct extends StatelessWidget {
  final ProductDto product;
  final UserDto currentUser;
  final VoidCallback update;
  final VoidCallback delete;
  final VoidCallback addToCart;
  final VoidCallback removeFromCart;

  const FullProduct({
    Key? key,
    required this.product,
    required this.currentUser,
    required this.update,
    required this.delete,
    required this.addToCart,
    required this.removeFromCart,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (product.pictures.isNotEmpty) ...[
          ImageCarousel(images: product.pictures),
          const TinySpacer(),
        ],
        Text(
          product.title,
          style: const TextStyle(fontWeight: FontWeight.w500),
        ),
        const TinySpacer(),
        Text(
          product.price.toString() + '\$',
          style: const TextStyle(fontWeight: FontWeight.w500),
        ),
        const TinySpacer(),
        Text(product.description),
        const InputSpacer(),
        Row(
          children: [
            OptionalImage(link: product.seller.avatar?.url, size: 50),
            const TinySpacer(horizontal: true),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  product.seller.name,
                  style: const TextStyle(fontWeight: FontWeight.w500),
                ),
                const TinySpacer(),
                Text(product.seller.phoneNumber),
              ],
            ),
          ],
        ),
        const InputSpacer(),
        if (currentUser.id == product.seller.id) Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ElevatedButton(
              onPressed: update,
              child: const Text('Update'),
            ),
            OutlinedButton(
              onPressed: delete,
              child: const Text('Delete'),
            )
          ],
        )
        else Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ElevatedButton(
              onPressed: product.isInCart ? removeFromCart : addToCart,
              child: Text(product.isInCart ? 'Remove from cart' : 'Add to cart'),
            ),
            OutlinedButton(
              onPressed: () => PhoneHelper.makeCall(product.seller.phoneNumber),
              child: const Text('Call seller'),
            ),
          ],
        ),
      ],
    );
  }
}
