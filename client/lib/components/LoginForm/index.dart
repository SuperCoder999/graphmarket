import 'package:flutter/material.dart';
import 'package:marketplace/components/Spacer/FormSpacer/index.dart';
import 'package:marketplace/components/Spacer/InputSpacer/index.dart';
import 'package:marketplace/components/PasswordInput/index.dart';
import 'package:marketplace/dtos/user/user-credentials.dart';
import 'package:marketplace/helpers/validation.dart';

typedef OnLoginSubmit = void Function(UserCredentialsDto);

class LoginForm extends StatefulWidget {
  final OnLoginSubmit onSubmit;
  const LoginForm({ Key? key, required this.onSubmit }) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final UserCredentialsDto _data = UserCredentialsDto(email: '', password: '');
  final GlobalKey<FormState> _form = GlobalKey<FormState>();

  void setEmail(String? value) {
    _data.email = value ?? '';
  }

  void setPassword(String? value) {
    _data.password = value ?? '';
  }

  void submit() {
    final bool isValid = _form.currentState!.validate();

    if (!isValid) {
      return;
    }

    _form.currentState!.save();
    widget.onSubmit(_data);
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _form,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        children: [
          const FormSpacer(),
          FormField<String>(
            builder: (field) {
              return TextField(
                onChanged: (value) => field.didChange(value),
                decoration: InputDecoration(
                  label: const Text('Email'),
                  errorText: field.errorText,
                  suffixIcon: const Icon(Icons.email),
                ),
              );
            },
            onSaved: setEmail,
            validator: Validation.email,
          ),
          const InputSpacer(),
          FormField<String>(
            builder: (field) {
              return PasswordInput(
                onChanged: (value) => field.didChange(value),
                label: const Text('Password'),
                errorText: field.errorText,
              );
            },
            onSaved: setPassword,
            validator: Validation.exists,
          ),
          const FormSpacer(),
          ElevatedButton(
            onPressed: submit,
            child: const Text('Log in')
          )
        ],
      ),
    );
  }
}
