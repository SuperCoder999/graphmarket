import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:marketplace/dtos/picture/picure.dart';

class ImageCarousel extends StatelessWidget {
  final List<PictureDto> images;
  const ImageCarousel({ Key? key, required this.images }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      options: CarouselOptions(
        height: 200,
        enlargeCenterPage: true,
        autoPlay: true,
        autoPlayInterval: const Duration(seconds: 3),
      ),
      items: images
        .map((image) => Image.network(image.url))
        .toList(),
    );
  }
}
