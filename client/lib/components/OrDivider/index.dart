import 'package:flutter/material.dart';

class OrDivider extends StatelessWidget {
  const OrDivider({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.symmetric(vertical: 3),
      child: Text('OR'),
    );
  }
}
