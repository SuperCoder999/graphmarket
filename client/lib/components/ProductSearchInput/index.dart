import 'package:flutter/material.dart';

typedef OnSearched = void Function(String);

class ProductSearchInput extends StatefulWidget {
  final OnSearched onSearched;
  const ProductSearchInput({ Key? key, required this.onSearched }) : super(key: key);

  @override
  _ProductSearchInputState createState() => _ProductSearchInputState();
}

class _ProductSearchInputState extends State<ProductSearchInput> {
  String _search = '';

  void setSearch(String? value) {
    _search = value ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      onChanged: setSearch,
      decoration: InputDecoration(
        label: const Text('Search'),
        suffixIcon: IconButton(
          icon: const Icon(Icons.search),
          splashRadius: 20,
          onPressed: () => widget.onSearched(_search),
        ),
      ),
    );
  }
}
