import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:marketplace/containers/App/index.dart';

void main() async {
  await dotenv.load();
  runApp(const Marketplace());
}
