import 'package:graphql/client.dart';
import 'package:marketplace/dtos/user/user-credentials.dart';
import 'package:marketplace/dtos/user/user-register.dart';
import 'package:marketplace/dtos/user/user.dart';
import 'package:marketplace/graphql/auth.dart';
import 'package:marketplace/graphql/client.dart';
import 'package:marketplace/helpers/storage.dart';

class UserService {
  static Future setToken(String token) async {
    await Storage.set(StorageKey.userToken, token);
  }

  static Future<UserDto?> getDetails() async {
    Map<String, dynamic>? result = await graphQL.query(
      QueryOptions(document: AuthQueries.getUser)
    );

    if (result == null || result['user'] == null) {
      return null;
    }

    return UserDto.fromJson(result['user']);
  }

  static Future<UserDto?> login(UserCredentialsDto data) async {
    Map<String, dynamic>? result = await graphQL.mutate(
      MutationOptions(
        document: AuthQueries.login,
        variables: data.toJson(),
      )
    );

    if (result == null || result['login'] == null) {
      return null;
    }

    await setToken(result['login']['token']);
    return UserDto.fromJson(result['login']);
  }

  static Future<UserDto?> register(UserRegisterDto data) async {
    Map<String, dynamic>? result = await graphQL.mutate(
      MutationOptions(
        document: AuthQueries.register,
        variables: await data.toJson(),
      )
    );

    if (result == null || result['register'] == null) {
      return null;
    }

    await setToken(result['register']['token']);
    return UserDto.fromJson(result['register']);
  }

  static Future<void> logout() async {
    await Storage.remove(StorageKey.userToken);
  }
}
