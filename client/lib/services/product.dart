import 'package:graphql/client.dart';
import 'package:marketplace/dtos/product/create-product.dart';
import 'package:marketplace/dtos/product/product-paginated-list.dart';
import 'package:marketplace/dtos/product/update-product.dart';
import 'package:marketplace/dtos/product/product-filter.dart';
import 'package:marketplace/dtos/product/product-list.dart';
import 'package:marketplace/dtos/product/product.dart';
import 'package:marketplace/graphql/cart.dart';
import 'package:marketplace/graphql/client.dart';
import 'package:marketplace/graphql/products.dart';

class ProductService {
  static Future<ProductPaginatedListDto?> getProducts([ProductFilterDto? filter]) async {
    final Map<String, dynamic>? result = await graphQL.query(
      QueryOptions(
        document: ProductQueries.getAll,
        variables: filter?.toJson() ?? {},
      ),
    );

    if (result == null || result['products'] == null) {
      return null;
    }

    return ProductPaginatedListDto.fromJson(result['products']);
  }

  static Future<ProductDto?> getProduct(String id) async {
    final Map<String, dynamic>? result = await graphQL.query(
      QueryOptions(
        document: ProductQueries.getExpanded,
        variables: {'id': id},
      ),
    );

    if (result == null || result['product'] == null) {
      return null;
    }

    return ProductDto.fromJson(result['product']);
  }

  static Future<ProductDto?> createProduct(CreateProductDto data) async {
    final Map<String, dynamic>? result = await graphQL.mutate(
      MutationOptions(
        document: ProductQueries.create,
        variables: await data.toJson(),
      ),
    );

    if (result == null || result['createProduct'] == null) {
      return null;
    }

    return ProductDto.fromJson(result['createProduct']);
  }

  static Future<ProductDto?> updateProduct(UpdateProductDto data) async {
    final Map<String, dynamic>? result = await graphQL.mutate(
      MutationOptions(
        document: ProductQueries.update,
        variables: await data.toJson(),
      ),
    );

    if (result == null || result['updateProduct'] == null) {
      return null;
    }

    return ProductDto.fromJson(result['updateProduct']);
  }

  static Future<String?> deleteProduct(String id) async {
    final Map<String, dynamic>? result = await graphQL.query(
      QueryOptions(
        document: ProductQueries.delete,
        variables: {'id': id},
      ),
    );

    if (result == null || result['deleteProduct'] == null) {
      return null;
    }

    return result['deleteProduct'];
  }

  static Future<List<ProductListDto>?> getCart() async {
    final Map<String, dynamic>? result = await graphQL.query(
      QueryOptions(document: CartQueries.readCart),
    );

    if (result == null || result['cart'] == null) {
      return null;
    }

    return (result['cart'] as List)
      .map((product) => ProductListDto.fromJson(product))
      .toList();
  }

  static Future<ProductDto?> addToCart(String id) async {
    final Map<String, dynamic>? result = await graphQL.query(
      QueryOptions(
        document: CartQueries.addToCart,
        variables: {'id': id},
      ),
    );

    if (result == null || result['addToCart'] == null) {
      return null;
    }

    return ProductDto.fromJson(result['addToCart']);
  }

  static Future<String?> removeFromCart(String id) async {
    final Map<String, dynamic>? result = await graphQL.query(
      QueryOptions(
        document: CartQueries.removeFromCart,
        variables: {'id': id},
      ),
    );

    if (result == null || result['removeFromCart'] == null) {
      return null;
    }

    return result['removeFromCart'];
  }
}
