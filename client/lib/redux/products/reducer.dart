import 'package:marketplace/dtos/product/product-list.dart';
import 'package:marketplace/dtos/product/product.dart';
import 'package:marketplace/redux/products/action-types.dart';
import 'package:marketplace/redux/reducer.dart';
import 'package:marketplace/redux/state.dart';

final Reducer productsReducer = createReducer({
  ProductsPending: (state, action) {
    return state.cloneWith(
      products: state.products.cloneWith(loading: true),
    );
  },
  SetProducts: (state, action) {
    action = action as SetProducts;

    if (action.payload == null) {
      return state.cloneWith(
        products: state.products.cloneWith(
          loading: false,
        ),
      );
    }

    return state.cloneWith(
      products: state.products.cloneWith(
        loading: false,
        products: action.payload!.products,
        productsLoaded: true,
        hasMoreProducts: action.payload!.hasMoreProducts,
      ),
    );
  },
  SetNextProducts: (state, action) {
    action = action as SetNextProducts;

    if (action.payload == null) {
      return state.cloneWith(
        products: state.products.cloneWith(
          loading: false,
        ),
      );
    }

    return state.cloneWith(
      products: state.products.cloneWith(
        loading: false,
        products: [
          ...state.products.products,
          ...action.payload!.products,
        ],
        productsLoaded: true,
        hasMoreProducts: action.payload!.hasMoreProducts,
      ),
    );
  },
  SetProductsFilter: (state, action) {
    action = action as SetProductsFilter;

    return state.cloneWith(
      products: state.products.cloneWith(
        filter: action.payload,
      ),
    );
  },
  SetExpandedProduct: (state, action) {
    action = action as SetExpandedProduct;

    return state.cloneWith(
      products: state.products.cloneWith(
        loading: false,
        expandedProduct: action.payload,
      ),
    );
  },
  SetUpdatingProduct: (state, action) {
    return state.cloneWith(
      products: state.products.cloneWith(
        updatingProduct: state.products.expandedProduct,
      ),
    );
  },
  RemoveExpandedProduct: (state, action) {
    return state.cloneWith(
      products: state.products.cloneWithNoExpanded(),
    );
  },
  RemoveUpdatingProduct: (state, action) {
    return state.cloneWith(
      products: state.products.cloneWithNoUpdating(),
    );
  },
  AddProduct: (state, action) {
    action = action as AddProduct;

    if (action.payload == null) {
      return state.cloneWith(
        products: state.products.cloneWith(
          loading: false,
        ),
      );
    }

    return state.cloneWith(
      products: state.products.cloneWith(
        loading: false,
        products: [
          ...state.products.products,
          ProductListDto.fromProductDto(action.payload!)
        ],
      ),
    );
  },
  ReplaceProduct: (state, action) {
    action = action as ReplaceProduct;

    final MarketplaceState failedState = state.cloneWith(
        products: state.products.cloneWith(
          loading: false,
        ),
      );

    if (action.payload == null) {
      return failedState;
    }

    int index = state.products.products.indexWhere(
      (product) => product.id == action.payload.id
    );

    if (index == -1) {
      return failedState;
    }

    final List<ProductListDto> newProducts = [...state.products.products];
    newProducts[index] = ProductListDto.fromProductDto(action.payload!);

    return state.cloneWith(
      products: state.products.cloneWithNoUpdating(
        loading: false,
        products: newProducts,
        expandedProduct: action.payload?.id == state.products.expandedProduct?.id
          ? action.payload!
          : null,
      ),
    );
  },
  RemoveProduct: (state, action) {
    action = action as RemoveProduct;

    final MarketplaceState failedState = state.cloneWith(
        products: state.products.cloneWith(
          loading: false,
        ),
      );

    if (action.payload == null) {
      return failedState;
    }

    final List<ProductListDto> newProducts = [...state.products.products];
    newProducts.removeWhere((product) => product.id == action.payload);

    if (action.payload == state.products.expandedProduct?.id) {
      state.cloneWith(
        products: state.products.cloneWithNoExpanded(
          loading: false,
          products: newProducts,
        ),
      );
    }

    return state.cloneWith(
      products: state.products.cloneWith(
        loading: false,
        products: newProducts,
      ),
    );
  },
  SetCart: (state, action) {
    action = action as SetCart;

    return state.cloneWith(
      products: state.products.cloneWith(
        cart: action.payload,
        cartLoaded: action.payload != null,
        loading: false,
      ),
    );
  },
  AddToCart: (state, action) {
    action = action as AddToCart;

    if (action.payload == null) {
      return state.cloneWith(
        products: state.products.cloneWith(
          loading: false,
        ),
      );
    }

    ProductDto? expanded = state.products.expandedProduct;

    List<ProductListDto> newProducts = state.products.products
      .map((product) => product.id == action.payload!.id
        ? ProductListDto.fromProductDto(action.payload!.cloneWith(isInCart: true))
        : product)
      .toList();

    return state.cloneWith(
      products: state.products.cloneWith(
        products: newProducts,
        cart: [...state.products.cart, ProductListDto.fromProductDto(action.payload!)],
        expandedProduct: expanded?.id == action.payload!.id
          ? expanded!.cloneWith(isInCart: true)
          : null,
        loading: false,
      ),
    );
  },
  RemoveFromCart: (state, action) {
    action = action as RemoveFromCart;

    if (action.payload == null) {
      return state.cloneWith(
        products: state.products.cloneWith(
          loading: false,
        ),
      );
    }

    ProductDto? expanded = state.products.expandedProduct;

    List<ProductListDto> newProducts = state.products.products
      .map((product) => product.id == action.payload!
        ? product.cloneWith(isInCart: false)
        : product)
      .toList();

    return state.cloneWith(
      products: state.products.cloneWith(
        products: newProducts,
        cart: state.products.cart
          .where((product) => product.id != action.payload!)
          .toList(),
        expandedProduct: expanded?.id == action.payload!
          ? expanded!.cloneWith(isInCart: false)
          : null,
        loading: false,
      ),
    );
  },
});
