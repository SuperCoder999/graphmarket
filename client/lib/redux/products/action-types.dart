import 'package:marketplace/dtos/product/product-filter.dart';
import 'package:marketplace/dtos/product/product-list.dart';
import 'package:marketplace/dtos/product/product-paginated-list.dart';
import 'package:marketplace/dtos/product/product.dart';
import 'package:marketplace/redux/action-types.dart';

class ProductsPending {}

class SetProducts extends ReduxPayloadAction<ProductPaginatedListDto?> {
  const SetProducts(ProductPaginatedListDto? payload) : super(payload);
}

class SetNextProducts extends ReduxPayloadAction<ProductPaginatedListDto?> {
  const SetNextProducts(ProductPaginatedListDto? payload) : super(payload);
}

class SetProductsFilter extends ReduxPayloadAction<ProductFilterDto> {
  const SetProductsFilter(ProductFilterDto payload) : super(payload);
}

class SetExpandedProduct extends ReduxPayloadAction<ProductDto?> {
  const SetExpandedProduct(ProductDto? payload) : super(payload);
}

class RemoveExpandedProduct {}

class SetUpdatingProduct {}

class RemoveUpdatingProduct {}

class AddProduct extends ReduxPayloadAction<ProductDto?> {
  const AddProduct(ProductDto? payload) : super(payload);
}

class ReplaceProduct extends ReduxPayloadAction<ProductDto?> {
  const ReplaceProduct(ProductDto? payload) : super(payload);
}

class RemoveProduct extends ReduxPayloadAction<String?> {
  const RemoveProduct(String? payload) : super(payload);
}

class SetCart extends ReduxPayloadAction<List<ProductListDto>?> {
  const SetCart(List<ProductListDto>? payload) : super(payload);
}

class AddToCart extends ReduxPayloadAction<ProductDto?> {
  const AddToCart(ProductDto? payload) : super(payload);
}

class RemoveFromCart extends ReduxPayloadAction<String?> {
  const RemoveFromCart(String? payload) : super(payload);
}
