import 'package:marketplace/dtos/product/product-filter.dart';
import 'package:marketplace/dtos/product/product-list.dart';
import 'package:marketplace/dtos/product/product.dart';
import 'package:marketplace/helpers/clonable.dart';

class ProductsState implements Clonable<ProductsState> {
  final List<ProductListDto> products;
  final ProductFilterDto filter;
  final bool productsLoaded;
  final bool hasMoreProducts;
  final ProductDto? expandedProduct;
  final ProductDto? updatingProduct;
  final List<ProductListDto> cart;
  final bool cartLoaded;
  final bool loading;

  const ProductsState({
    this.products = const [],
    this.filter = const ProductFilterDto.initial(),
    this.productsLoaded = false,
    this.hasMoreProducts = true,
    this.expandedProduct,
    this.updatingProduct,
    this.cart = const [],
    this.cartLoaded = false,
    this.loading = false,
  });

  @override
  ProductsState cloneWith({
    List<ProductListDto>? products,
    ProductFilterDto? filter,
    bool? productsLoaded,
    bool? hasMoreProducts,
    ProductDto? expandedProduct,
    ProductDto? updatingProduct,
    List<ProductListDto>? cart,
    bool? cartLoaded,
    bool? loading,
  }) => ProductsState(
    products: products ?? this.products,
    filter: filter ?? this.filter,
    productsLoaded: productsLoaded ?? this.productsLoaded,
    hasMoreProducts: hasMoreProducts ?? this.hasMoreProducts,
    expandedProduct: expandedProduct ?? this.expandedProduct,
    updatingProduct: updatingProduct ?? this.updatingProduct,
    cart: cart ?? this.cart,
    cartLoaded: cartLoaded ?? this.cartLoaded,
    loading: loading ?? this.loading,
  );

  ProductsState cloneWithNoExpanded({
    List<ProductListDto>? products,
    ProductFilterDto? filter,
    bool? productsLoaded,
    bool? hasMoreProducts,
    ProductDto? updatingProduct,
    List<ProductListDto>? cart,
    bool? cartLoaded,
    bool? loading,
  }) => ProductsState(
    products: products ?? this.products,
    filter: filter ?? this.filter,
    productsLoaded: productsLoaded ?? this.productsLoaded,
    hasMoreProducts: hasMoreProducts ?? this.hasMoreProducts,
    updatingProduct: updatingProduct ?? this.updatingProduct,
    cart: cart ?? this.cart,
    cartLoaded: cartLoaded ?? this.cartLoaded,
    loading: loading ?? this.loading,
  );

  ProductsState cloneWithNoUpdating({
    List<ProductListDto>? products,
    ProductFilterDto? filter,
    bool? productsLoaded,
    bool? hasMoreProducts,
    ProductDto? expandedProduct,
    List<ProductListDto>? cart,
    bool? cartLoaded,
    bool? loading,
  }) => ProductsState(
    products: products ?? this.products,
    filter: filter ?? this.filter,
    productsLoaded: productsLoaded ?? this.productsLoaded,
    hasMoreProducts: hasMoreProducts ?? this.hasMoreProducts,
    expandedProduct: expandedProduct ?? this.expandedProduct,
    cart: cart ?? this.cart,
    cartLoaded: cartLoaded ?? this.cartLoaded,
    loading: loading ?? this.loading,
  );
}
