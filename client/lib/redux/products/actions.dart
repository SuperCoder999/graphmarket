import 'package:marketplace/dtos/product/create-product.dart';
import 'package:marketplace/dtos/product/product-filter.dart';
import 'package:marketplace/dtos/product/product-list.dart';
import 'package:marketplace/dtos/product/product-paginated-list.dart';
import 'package:marketplace/dtos/product/product.dart';
import 'package:marketplace/dtos/product/update-product.dart';
import 'package:marketplace/redux/products/action-types.dart';
import 'package:marketplace/redux/state.dart';
import 'package:marketplace/services/product.dart';
import 'package:redux_thunk/redux_thunk.dart';

class Products {
  static ThunkAction<MarketplaceState> reloadProducts(String search) {
    return (store) async {
      final ProductFilterDto prevFilter = store.state.products.filter;
      final ProductFilterDto filter = prevFilter.cloneWith(page: 1, search: search);

      store.dispatch(ProductsPending());
      store.dispatch(SetProductsFilter(filter));

      final ProductPaginatedListDto? products = await ProductService.getProducts(filter);
      store.dispatch(SetProducts(products));
    };
  }

  static ThunkAction<MarketplaceState> loadMoreProducts() {
    return (store) async {
      if (!store.state.products.hasMoreProducts) {
        return;
      }

      final ProductFilterDto prevFilter = store.state.products.filter;
      final ProductFilterDto filter = prevFilter.cloneWith(page: prevFilter.page + 1);

      store.dispatch(ProductsPending());
      store.dispatch(SetProductsFilter(filter));

      final ProductPaginatedListDto? products = await ProductService.getProducts(filter);
      store.dispatch(SetNextProducts(products));
    };
  }

  static ThunkAction<MarketplaceState> loadExpandedProduct(String id) {
    return (store) async {
      store.dispatch(ProductsPending());
      final ProductDto? product = await ProductService.getProduct(id);
      store.dispatch(SetExpandedProduct(product));
    };
  }

  static ThunkAction<MarketplaceState> create(CreateProductDto data) {
    return (store) async {
      store.dispatch(ProductsPending());
      final ProductDto? product = await ProductService.createProduct(data);
      store.dispatch(AddProduct(product));
    };
  }

  static ThunkAction<MarketplaceState> update(UpdateProductDto data) {
    return (store) async {
      store.dispatch(ProductsPending());
      final ProductDto? product = await ProductService.updateProduct(data);
      store.dispatch(ReplaceProduct(product));
    };
  }

  static ThunkAction<MarketplaceState> delete(String id) {
    return (store) async {
      store.dispatch(ProductsPending());
      final String? saved = await ProductService.deleteProduct(id);
      store.dispatch(RemoveProduct(saved));
    };
  }

  static ThunkAction<MarketplaceState> loadCart() {
    return (store) async {
      store.dispatch(ProductsPending());
      final List<ProductListDto>? cart = await ProductService.getCart();
      store.dispatch(SetCart(cart));
    };
  }

  static ThunkAction<MarketplaceState> addToCart(String id) {
    return (store) async {
      store.dispatch(ProductsPending());
      final ProductDto? addedProduct = await ProductService.addToCart(id);
      store.dispatch(AddToCart(addedProduct));
    };
  }

  static ThunkAction<MarketplaceState> removeFromCart(String id) {
    return (store) async {
      store.dispatch(ProductsPending());
      final String? removedProduct = await ProductService.removeFromCart(id);
      store.dispatch(RemoveFromCart(removedProduct));
    };
  }
}
