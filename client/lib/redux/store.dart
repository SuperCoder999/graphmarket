import 'package:marketplace/redux/reducer.dart';
import 'package:marketplace/redux/state.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

class MarketplaceStore extends Store<MarketplaceState> {
  MarketplaceStore() : super(
    marketplaceReducer,
    initialState: const MarketplaceState(),
    middleware: const [thunkMiddleware],
  );
}

final MarketplaceStore marketplaceStore = MarketplaceStore();
