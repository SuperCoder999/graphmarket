import 'package:marketplace/dtos/user/user.dart';
import 'package:marketplace/helpers/clonable.dart';

class AuthState implements Clonable<AuthState> {
  final UserDto? user;
  final bool loading;
  final bool userLoaded;

  const AuthState({
    this.user,
    this.loading = false,
    this.userLoaded = false
  });

  @override
  AuthState cloneWith({
    UserDto? user,
    bool? loading,
    bool? userLoaded,
  }) => AuthState(
    user: user ?? this.user,
    loading: loading ?? this.loading,
    userLoaded: userLoaded ?? this.userLoaded,
  );

  AuthState cloneWithNoUser({
    bool? loading,
    bool? userLoaded,
  }) => AuthState(
    user: null,
    loading: loading ?? this.loading,
    userLoaded: userLoaded ?? this.userLoaded
  );
}
