import 'package:marketplace/dtos/user/user.dart';
import 'package:marketplace/redux/action-types.dart';

class AuthPending {}

class LoadUserSuccess extends ReduxPayloadAction<UserDto?> {
  const LoadUserSuccess(UserDto? payload) : super(payload);
}
