import 'package:marketplace/dtos/user/user-credentials.dart';
import 'package:marketplace/dtos/user/user-register.dart';
import 'package:marketplace/dtos/user/user.dart';
import 'package:marketplace/redux/auth/action-types.dart';
import 'package:marketplace/redux/state.dart';
import 'package:marketplace/services/user.dart';
import 'package:redux_thunk/redux_thunk.dart';

class Auth {
  static ThunkAction<MarketplaceState> loadUser() {
    return (store) async {
      store.dispatch(AuthPending());
      UserDto? user = await UserService.getDetails();
      store.dispatch(LoadUserSuccess(user));
    };
  }

  static ThunkAction<MarketplaceState> login(UserCredentialsDto data) {
    return (store) async {
      store.dispatch(AuthPending());
      UserDto? user = await UserService.login(data);
      store.dispatch(LoadUserSuccess(user));
    };
  }

  static ThunkAction<MarketplaceState> register(UserRegisterDto data) {
    return (store) async {
      store.dispatch(AuthPending());
      UserDto? user = await UserService.register(data);
      store.dispatch(LoadUserSuccess(user));
    };
  }

  static ThunkAction<MarketplaceState> logout() {
    return (store) async {
      store.dispatch(AuthPending());
      await UserService.logout();
      store.dispatch(const LoadUserSuccess(null));
    };
  }
}
