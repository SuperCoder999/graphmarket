import 'package:marketplace/redux/auth/action-types.dart';
import 'package:marketplace/redux/reducer.dart';

final Reducer authReducer = createReducer({
  AuthPending: (state, action) {
    action = action as AuthPending;

    return state.cloneWith(
      auth: state.auth.cloneWith(
        loading: true,
      ),
    );
  },
  LoadUserSuccess: (state, action) {
    action = action as LoadUserSuccess;

    if (action.payload == null) {
      return state.cloneWith(
        auth: state.auth.cloneWithNoUser(
          loading: false,
          userLoaded: true,
        ),
      );
    }

    return state.cloneWith(
      auth: state.auth.cloneWith(
        user: action.payload,
        loading: false,
        userLoaded: true,
      ),
    );
  },
});
