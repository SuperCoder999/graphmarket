import 'package:marketplace/redux/auth/reducer.dart';
import 'package:marketplace/redux/products/reducer.dart';
import 'package:marketplace/redux/state.dart';
import 'package:redux/redux.dart';

typedef ActionHandler<State> = MarketplaceState Function(MarketplaceState, dynamic);
typedef Reducer = MarketplaceState Function(MarketplaceState, dynamic);

Reducer createReducer(Map<Type, ActionHandler> handlersMap) {
  return (state, action) {
    Type type = action.runtimeType;

    if (handlersMap.containsKey(type)) {
      ActionHandler handler = handlersMap[type]!;
      return handler(state, action);
    }

    return state;
  };
}

final Reducer marketplaceReducer = combineReducers<MarketplaceState>([
  authReducer,
  productsReducer,
]);
