import 'package:marketplace/helpers/clonable.dart';
import 'package:marketplace/redux/auth/state.dart';
import 'package:marketplace/redux/products/state.dart';

class MarketplaceState implements Clonable<MarketplaceState> {
  final AuthState auth;
  final ProductsState products;

  const MarketplaceState({
    this.auth = const AuthState(),
    this.products = const ProductsState(),
  });

  @override
  MarketplaceState cloneWith({
    AuthState? auth,
    ProductsState? products,
  }) => MarketplaceState(
    auth: auth ?? this.auth,
    products: products ?? this.products,
  );
}
