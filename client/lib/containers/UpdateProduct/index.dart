import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:marketplace/components/UpdateProductForm/index.dart';
import 'package:marketplace/dtos/product/product.dart';
import 'package:marketplace/dtos/product/update-product.dart';
import 'package:marketplace/navigators/MarketNavigator/index.dart';
import 'package:marketplace/redux/products/actions.dart';
import 'package:marketplace/redux/state.dart';

class _UpdateInfo {
  final OnUpdateProductSubmit update;
  final ProductDto existing;

  const _UpdateInfo({
    required this.update,
    required this.existing,
  });
}

class UpdateProduct extends StatelessWidget {
  const UpdateProduct({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<MarketplaceState, _UpdateInfo>(
      converter: (store) => _UpdateInfo(
        update: (UpdateProductDto data) {
          WidgetsBinding.instance?.addPostFrameCallback((_) {
            MarketNavigator.of(context).setSelectedIndex(0);
          });
          
          store.dispatch(Products.update(data));
        },
        existing: store.state.products.updatingProduct!,
      ),
      builder: (context, info) {
        return SingleChildScrollView(
          child: UpdateProductForm(
            onSubmit: info.update,
            existing: info.existing,
          ),
        );
      },
    );
  }
}
