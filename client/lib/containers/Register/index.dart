import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:marketplace/components/OrDivider/index.dart';
import 'package:marketplace/components/RegisterForm/index.dart';
import 'package:marketplace/dtos/user/user-register.dart';
import 'package:marketplace/redux/auth/actions.dart';
import 'package:marketplace/redux/state.dart';

class Register extends StatelessWidget {
  const Register({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final NavigatorState navigator = Navigator.of(context);

    return StoreConnector<MarketplaceState, OnRegisterSubmit>(
      converter: (store) => (UserRegisterDto data) => store.dispatch(Auth.register(data)),
      builder: (context, register) {
        return SingleChildScrollView(
          child: Column(
            children: [
              RegisterForm(onSubmit: register),
              const OrDivider(),
              TextButton(
                onPressed: () => navigator.pop(),
                child: const Text('Login'),
              ),
            ],
          ),
        );
      },
    );
  }
}
