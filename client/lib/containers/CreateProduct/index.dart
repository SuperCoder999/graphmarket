import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:marketplace/components/CreateProductForm/index.dart';
import 'package:marketplace/dtos/product/create-product.dart';
import 'package:marketplace/navigators/MarketNavigator/index.dart';
import 'package:marketplace/redux/products/actions.dart';
import 'package:marketplace/redux/state.dart';

class CreateProduct extends StatelessWidget {
  const CreateProduct({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<MarketplaceState, OnCreateProductSubmit>(
      converter: (store) => (CreateProductDto data) {
        WidgetsBinding.instance?.addPostFrameCallback((_) {
          MarketNavigator.of(context).setSelectedIndex(0);
        });

        store.dispatch(Products.create(data));
      },
      builder: (context, create) {
        return SingleChildScrollView(
          child: CreateProductForm(onSubmit: create),
        );
      },
    );
  }
}
