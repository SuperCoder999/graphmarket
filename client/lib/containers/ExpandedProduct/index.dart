import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:marketplace/components/FullProduct/index.dart';
import 'package:marketplace/components/Spinner/index.dart';
import 'package:marketplace/dtos/product/product.dart';
import 'package:marketplace/dtos/user/user.dart';
import 'package:marketplace/navigators/MarketNavigator/index.dart';
import 'package:marketplace/redux/products/action-types.dart';
import 'package:marketplace/redux/products/actions.dart';
import 'package:marketplace/redux/state.dart';
import 'package:marketplace/redux/store.dart';

class _ExpandedInfo {
  final ProductDto? expanded;
  final UserDto currentUser;
  final VoidCallback update;
  final VoidCallback delete;
  final VoidCallback addToCart;
  final VoidCallback removeFromCart;
  final bool loading;

  const _ExpandedInfo({
    this.expanded,
    required this.currentUser,
    required this.update,
    required this.delete,
    required this.loading,
    required this.addToCart,
    required this.removeFromCart,
  });
}

class ExpandedProduct extends StatefulWidget {
  const ExpandedProduct({ Key? key }) : super(key: key);

  @override
  _ExpandedProductState createState() => _ExpandedProductState();
}

class _ExpandedProductState extends State<ExpandedProduct> {
  bool _hasProduct = false;

  @override
  void dispose() {
    super.dispose();
    marketplaceStore.dispatch(RemoveExpandedProduct());
  }

  @override
  Widget build(BuildContext context) {
    final NavigatorState navigator = Navigator.of(context);

    void goBack() {
      WidgetsBinding.instance?.addPostFrameCallback((_) {
        navigator.pop();
        marketplaceStore.dispatch(RemoveExpandedProduct());
      });
    }

    return StoreConnector<MarketplaceState, _ExpandedInfo>(
      converter: (store) {
        final ProductDto? expanded = store.state.products.expandedProduct;

        return _ExpandedInfo(
          expanded: expanded,
          currentUser: store.state.auth.user!,
          update: () {
            WidgetsBinding.instance?.addPostFrameCallback((_) {
              MarketNavigator.of(context).setSelectedIndex(1);
            });

            store.dispatch(SetUpdatingProduct());
          },
          delete: () {
            if (expanded == null) {
              return;
            }

            goBack();
            store.dispatch(Products.delete(expanded.id));
          },
          addToCart: () => store.state.products.expandedProduct == null
            ? null
            : store.dispatch(Products
                .addToCart(store.state.products.expandedProduct!.id)),
          removeFromCart: () => store.state.products.expandedProduct == null
            ? null
            : store.dispatch(Products.removeFromCart(store.state.products.expandedProduct!.id)),
          loading: store.state.products.loading,
        );
      },
      builder: (context, info) {
        if (info.expanded == null && !_hasProduct) {
          goBack();
          return Container();
        } else {
          _hasProduct = true;
        }

        return SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (info.loading) const Spinner(),
              if (info.expanded != null) FullProduct(
                product: info.expanded!,
                currentUser: info.currentUser,
                update: info.update,
                delete: info.delete,
                addToCart: info.addToCart,
                removeFromCart: info.removeFromCart,
              ),
              TextButton(
                onPressed: goBack,
                child: const Text('Back'),
              ),
            ],
          ),
        );
      },
    );
  }
}
