import 'package:flutter/material.dart';

class DefaultPageWrapper extends StatelessWidget {
  final Widget child;
  final String title;

  const DefaultPageWrapper({
    Key? key,
    required this.child,
    required this.title
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(title: Text(title)),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(20),
          constraints: BoxConstraints(
            maxHeight: height,
          ),
          child: child,
        ),
      ),
    );
  }
}
