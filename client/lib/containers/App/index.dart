import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:marketplace/constants/app-settings.dart';
import 'package:marketplace/navigators/RootNavigator/index.dart';
import 'package:marketplace/redux/store.dart';

class Marketplace extends StatelessWidget {
  const Marketplace({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: Settings.appName,
      theme: Settings.theme,
      home: StoreProvider(
        store: marketplaceStore,
        child: const RootNavigator(),
      ),
    );
  }
}
