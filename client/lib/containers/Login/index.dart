import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:marketplace/components/LoginForm/index.dart';
import 'package:marketplace/components/OrDivider/index.dart';
import 'package:marketplace/dtos/user/user-credentials.dart';
import 'package:marketplace/redux/auth/actions.dart';
import 'package:marketplace/redux/state.dart';

class Login extends StatelessWidget {
  const Login({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final NavigatorState navigator = Navigator.of(context);

    return StoreConnector<MarketplaceState, OnLoginSubmit>(
      converter: (store) => (UserCredentialsDto data) => store.dispatch(Auth.login(data)),
      builder: (context, login) {
        return SingleChildScrollView(
          child: Column(
            children: [
              LoginForm(onSubmit: login),
              const OrDivider(),
              TextButton(
                onPressed: () => navigator.pushNamed('AuthRegister'),
                child: const Text('Register'),
              ),
            ],
          ),
        );
      }
    );
  }
}
