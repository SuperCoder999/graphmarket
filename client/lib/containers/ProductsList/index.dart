import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:marketplace/components/ListProduct/index.dart';
import 'package:marketplace/components/ProductSearchInput/index.dart';
import 'package:marketplace/components/Spacer/TinySpacer/index.dart';
import 'package:marketplace/components/Spinner/index.dart';
import 'package:marketplace/dtos/product/product-list.dart';
import 'package:marketplace/dtos/product/product.dart';
import 'package:marketplace/dtos/user/user.dart';
import 'package:marketplace/redux/auth/actions.dart';
import 'package:marketplace/redux/products/actions.dart';
import 'package:marketplace/redux/state.dart';

typedef _ReloadProducts = void Function(String);
typedef _LoadExpanded = void Function(String);
typedef _AddToCart = void Function(String);
typedef _RemoveFromCart = void Function(String);

class _ProductsInfo {
  final VoidCallback loadProducts;
  final VoidCallback loadCart;
  final _ReloadProducts reloadProducts;
  final _LoadExpanded loadExpanded;
  final VoidCallback logout;
  final List<ProductListDto> products;
  final List<ProductListDto> cart;
  final ProductDto? expanded;
  final bool allLoaded;
  final bool cartLoaded;
  final _AddToCart addToCart;
  final _RemoveFromCart removeFromCart;
  final UserDto user;
  final bool loading;

  const _ProductsInfo({
    required this.loadProducts,
    required this.loadCart,
    required this.reloadProducts,
    required this.loadExpanded,
    required this.logout,
    required this.products,
    required this.cart,
    this.expanded,
    required this.allLoaded,
    required this.cartLoaded,
    required this.addToCart,
    required this.removeFromCart,
    required this.user,
    required this.loading,
  });
}

class ProductsList extends StatefulWidget {
  const ProductsList({ Key? key }) : super(key: key);

  @override
  _ProductsListState createState() => _ProductsListState();
}

class _ProductsListState extends State<ProductsList> {
  final ScrollController _scroll = ScrollController();
  bool _scrollListenerAdded = false;
  bool _isCart = false;
  bool _expandedOpen = false;

  void _addScrollListener(VoidCallback loadMore) {
    _scroll.addListener(() {
      final double max = _scroll.position.maxScrollExtent;
      final double current = _scroll.position.pixels;
      final double offset = max - current;

      if (offset <= 10) {
        loadMore();
      }
    });

    _scrollListenerAdded = true;
  }

  @override
  Widget build(BuildContext context) {
    final NavigatorState navigator = Navigator.of(context);

    return StoreConnector<MarketplaceState, _ProductsInfo>(
      converter: (store) => _ProductsInfo(
        loadProducts: () => store.dispatch(Products.loadMoreProducts()),
        loadCart: () => store.dispatch(Products.loadCart()),
        reloadProducts: (search) => store.dispatch(Products.reloadProducts(search)),
        loadExpanded: (id) => store.dispatch(Products.loadExpandedProduct(id)),
        logout: () => store.dispatch(Auth.logout()),
        products: store.state.products.products,
        cart: store.state.products.cart,
        expanded: store.state.products.expandedProduct,
        allLoaded: store.state.products.productsLoaded,
        cartLoaded: store.state.products.cartLoaded,
        addToCart: (id) => store.dispatch(Products.addToCart(id)),
        removeFromCart: (id) => store.dispatch(Products.removeFromCart(id)),
        user: store.state.auth.user!,
        loading: store.state.products.loading,
      ),
      builder: (context, info) {
        void switchMode() {
          setState(() {
            _isCart = !_isCart;
          });
        }

        bool loaded = _isCart ? info.cartLoaded : info.allLoaded;

        if (info.expanded != null) {
          if (!_expandedOpen) {
            WidgetsBinding.instance?.addPostFrameCallback((_) {
              navigator.popUntil(ModalRoute.withName('ProductsList'));
              navigator.pushNamed('ProductsListExpanded');
            });

            _expandedOpen = true;
          }

          return Container(); 
        }

        if (info.expanded == null) {
          _expandedOpen = false;
        }

        if (!_scrollListenerAdded && !_isCart) {
          _addScrollListener(info.loadProducts);
        }

        if (!info.loading && !loaded) {
          if (_isCart) {
            info.loadCart();
          } else {
            info.loadProducts();
          }
        }

        List<ProductListDto> products = _isCart ? info.cart : info.products;

        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            if (info.loading) const Spinner(),
            const TinySpacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextButton(
                  onPressed: info.logout,
                  child: const Text('Log out'),
                ),
                TextButton(
                  onPressed: switchMode,
                  child: Text('Show ${_isCart ? 'all products' : 'cart'}'),
                ),
              ],
            ),
            if (!_isCart) ProductSearchInput(onSearched: info.reloadProducts),
            const TinySpacer(),
            Flexible(
              child: ListView.separated(
                controller: _scroll,
                shrinkWrap: true,
                itemCount: products.length,
                itemBuilder: (context, index) {
                  final ProductListDto product = products[index];
                  final VoidCallback cartOperation = () => product.isInCart
                      ? info.removeFromCart(product.id)
                      : info.addToCart(product.id);

                  return ListProduct(
                    product: product,
                    onShowDetails: () => info.loadExpanded(product.id),
                    onCartButtonClick: product.sellerId == info.user.id ? null : cartOperation
                  );
                },
                separatorBuilder: (context, index) => const TinySpacer(),
              ),
            ),
          ],
        );
      },
    );
  }
}
