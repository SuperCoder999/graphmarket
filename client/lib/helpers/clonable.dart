abstract class Clonable<Self> {
  Self cloneWith();
}
