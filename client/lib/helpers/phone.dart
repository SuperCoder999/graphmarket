import 'package:url_launcher/url_launcher.dart';

class PhoneHelper {
  static void makeCall(String phoneNumber) {
    launch('tel://$phoneNumber');
  }
}
