import 'package:graphql/client.dart';
import 'package:marketplace/helpers/notifications.dart';

class ExceptionsHelper {
  static void handleGraphQLError(OperationException? exception) {
    if (exception != null) {
      String errorMessage = exception.graphqlErrors
        .map((err) => err.message)
        .join(', ');

      Notifications.error(errorMessage);
    }
  }
}
