class TextHelper {
  static String limitWords(String text, [int limit = 10]) {
    List<String> words = text.split(' ');

    if (words.length <= limit) {
      return text;
    }

    return words.take(limit).join(' ') + '...';
  }
}
