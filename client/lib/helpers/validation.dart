import 'package:validators/validators.dart' as validator;

class Validation {
  static String? exists(String? value) {
    bool isError = value == null || value.isEmpty;
    return isError ? 'Mustn\'t be empty' : null;
  }

  static String? email(String? value) {
    bool isError = value == null || !validator.isEmail(value);
    return isError ? 'Invalid email.' : null;
  }

  static String? phoneNumber(String? value) {
    bool isError = value == null || !RegExp(r'^\+\d{12}$').hasMatch(value);
    return isError ? 'Must be a valid phone number.' : null;
  }

  static String? password(String? value) {
    bool isError = value == null || value.length < 8;
    return isError ? 'Password must be at least 8 characters long.' : null;
  }

  static String? positive(num? value) {
    bool isError = value == null || value <= 0;
    return isError ? 'Must be positive.' : null;
  }
}
