import 'package:shared_preferences/shared_preferences.dart';

class StorageKey {
  static const String _namespace = 'com:marketplace:';
  static const String userToken = '${_namespace}user:token';
}

class Storage {
  static SharedPreferences? _instance;

  static Future<void> _ensureInstance() async {
    if (_instance != null) {
      return;
    }

    _instance = await SharedPreferences.getInstance();
  }

  static Future<String?> get(String key) async {
    await _ensureInstance();
    return _instance!.getString(key);
  }

  static Future<void> set(String key, String value) async {
    await _ensureInstance();
    await _instance!.setString(key, value);
  }

  static Future<void> remove(String key) async {
    await _ensureInstance();
    await _instance!.remove(key);
  }
}
