import 'package:flutter/material.dart';
import 'package:marketplace/navigators/ProductsListNavigator/index.dart';
import 'package:marketplace/pages/ModifyProductPage/index.dart';

class MarketNavigator extends StatefulWidget {
  const MarketNavigator({ Key? key }) : super(key: key);

  @override
  MarketNavigatorState createState() => MarketNavigatorState();

  static MarketNavigatorState of(BuildContext context) {
    return context.findAncestorStateOfType<MarketNavigatorState>()!;
  }
}

class MarketNavigatorState extends State<MarketNavigator> {
  final List<Widget> _widgets = [
    ProductsListNavigator(),
    const ModifyProductPage(),
  ];

  int _selectedIndex = 0;

  void setSelectedIndex(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    return Column(
      children: [
        Flexible(child: _widgets[_selectedIndex]),
        BottomNavigationBar(
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.list),
              label: 'Products',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.add_box_outlined),
              label: 'Add product',
            ),
          ],
          currentIndex: _selectedIndex,
          onTap: setSelectedIndex,
          selectedItemColor: theme.primaryColor,
        ),
      ],
    );
  }
}
