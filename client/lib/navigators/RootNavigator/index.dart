import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:marketplace/components/Spinner/index.dart';
import 'package:marketplace/navigators/AuthNavigator/index.dart';
import 'package:marketplace/navigators/MarketNavigator/index.dart';
import 'package:marketplace/redux/auth/actions.dart';
import 'package:marketplace/redux/auth/state.dart';
import 'package:marketplace/redux/state.dart';

typedef PageCreator = Widget Function();

class _AuthInfo {
  final AuthState auth;
  final VoidCallback loadUser;

  const _AuthInfo({
    required this.auth,
    required this.loadUser,
  });
}

class RootNavigator extends StatelessWidget {
  const RootNavigator({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<MarketplaceState, _AuthInfo>(
      converter: (store) => _AuthInfo(
        auth: store.state.auth,
        loadUser: () => store.dispatch(Auth.loadUser())
      ),
      builder: (context, info) { // Switch navigator :)
        if (!info.auth.userLoaded && !info.auth.loading) {
          info.loadUser();
        }
  
        if (!info.auth.userLoaded || info.auth.loading) {
          return const Spinner();
        }
  
        if (info.auth.user == null) {
          return AuthNavigator();
        }
  
        return const MarketNavigator();
      },
    );
  }
}
