import 'package:flutter/material.dart';
import 'package:marketplace/navigators/RootNavigator/index.dart';

class StackNavigator extends StatelessWidget {
  final String initialRoute;
  final Map<String, PageCreator> routes;

  const StackNavigator({
    Key? key,
    required this.initialRoute,
    required this.routes,
  }) : super(key: key);

  MaterialPageRoute createPage(String name) {
    return MaterialPageRoute(
      builder: (context) => routes[name]!(),
      settings: RouteSettings(name: name),
    );
  }

  MaterialPageRoute createSafePage(String? name) {
    if (name != null && routes.containsKey(name)) {
      return createPage(name);
    }

    return createPage(initialRoute);
  }

  @override
  Widget build(BuildContext context) {
    return Navigator(
      initialRoute: initialRoute,
      onGenerateRoute: (settings) => createSafePage(settings.name),
    );
  }
}
