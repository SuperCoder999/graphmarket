import 'package:flutter/material.dart';
import 'package:marketplace/navigators/StackNavigator/index.dart';
import 'package:marketplace/pages/ExpandedProductPage/index.dart';
import 'package:marketplace/pages/ProductsListPage/index.dart';

class ProductsListNavigator extends StackNavigator {
  ProductsListNavigator({ Key? key }) : super(
    key: key,
    initialRoute: 'ProductsList',
    routes: {
      'ProductsList': () => const ProductsListPage(),
      'ProductsListExpanded': () => const ExpandedProductPage(),
    },
  );
}
