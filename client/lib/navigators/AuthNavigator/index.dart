import 'package:flutter/material.dart';
import 'package:marketplace/navigators/StackNavigator/index.dart';
import 'package:marketplace/pages/LoginPage/index.dart';
import 'package:marketplace/pages/RegisterPage/index.dart';

class AuthNavigator extends StackNavigator {
  AuthNavigator({ Key? key }) : super(
    key: key,
    initialRoute: 'AuthLogin',
    routes: {
      'AuthLogin': () => const LoginPage(),
      'AuthRegister': () => const RegisterPage(),
    },
  );
}
