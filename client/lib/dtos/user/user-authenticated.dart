class UserAuthenticatedDto {
  String id;
  String token;
  String email;
  String name;
  String? role;

  UserAuthenticatedDto({
    required this.id,
    required this.token,
    required this.email,
    required this.name,
    this.role,
  });

  factory UserAuthenticatedDto.fromJson(Map<String, dynamic> json) {
    return UserAuthenticatedDto(
      id: json['id'],
      token: json['token'],
      email: json['email'],
      name: json['name'],
      role: json['role'],
    );
  }
}
