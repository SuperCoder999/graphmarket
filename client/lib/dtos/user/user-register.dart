import 'package:image_picker/image_picker.dart';
import 'package:marketplace/dtos/files/file.dart';

class UserRegisterDto {
  String email;
  String name;
  String phoneNumber;
  String password;
  XFile? avatar;

  UserRegisterDto({
    required this.email,
    required this.name,
    required this.phoneNumber,
    required this.password,
    this.avatar,
  });

  Future<Map<String, dynamic>> toJson() async => {
    'email': email,
    'name': name,
    'phoneNumber': phoneNumber,
    'password': password,
    'avatar': await FileDto.fromXFileToJson(avatar),
  };
}
