import 'package:marketplace/dtos/picture/picure.dart';

class SellerDto {
  String id;
  String name;
  String phoneNumber;
  PictureDto? avatar;

  SellerDto({
    required this.id,
    required this.name,
    required this.phoneNumber,
    this.avatar,
  });

  factory SellerDto.fromJson(Map<String, dynamic> json) {
    Map<String, dynamic>? avatar = json['avatar'];

    return SellerDto(
      id: json['id'],
      name: json['name'],
      phoneNumber: json['phoneNumber'],
      avatar: avatar == null ? null : PictureDto.fromJson(avatar),
    );
  }
}
