class UserCredentialsDto {
  String email;
  String password;

  UserCredentialsDto({
    required this.email,
    required this.password,
  });

  Map<String, dynamic> toJson() => {
    'email': email,
    'password': password,
  };
}
