class PictureDto {
  String id;
  String url;

  PictureDto({
    required this.id,
    required this.url,
  });

  factory PictureDto.fromJson(Map<String, dynamic> json) {
    return PictureDto(
      id: json['id'],
      url: json['url'],
    );
  }
}