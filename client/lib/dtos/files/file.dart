import 'dart:convert' as convert;
import 'package:image_picker/image_picker.dart';

class FileDto {
  final String filename;
  final String base64;

  const FileDto({
    required this.filename,
    required this.base64,
  });

  Map<String, dynamic> toJson() => {
    'filename': filename,
    'base64': base64,
  };

  static Future<FileDto> fromXFile(XFile file) async {
    return FileDto(
      filename: file.name,
      base64: convert.base64.encode(await file.readAsBytes()),
    );
  }

  static Future<Map<String, dynamic>?> fromXFileToJson(XFile? file) async {
    if (file == null) {
      return null;
    }

    final FileDto dto = await FileDto.fromXFile(file);
    return dto.toJson();
  }
}
