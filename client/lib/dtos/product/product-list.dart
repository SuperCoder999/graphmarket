import 'package:marketplace/dtos/picture/picure.dart';
import 'package:marketplace/dtos/product/product.dart';
import 'package:marketplace/helpers/clonable.dart';

class ProductListDto implements Clonable<ProductListDto> {
  String id;
  String title;
  double price;
  String description;
  bool isInCart;
  String sellerId;
  PictureDto? picture;

  ProductListDto({
    required this.id,
    required this.title,
    required this.price,
    required this.description,
    required this.isInCart,
    required this.sellerId,
    this.picture,
  });

  @override
  ProductListDto cloneWith({
    String? id,
    String? title,
    double? price,
    String? description,
    bool? isInCart,
    String? sellerId,
    PictureDto? picture,
  }) {
    return ProductListDto(
      id: id ?? this.id,
      title: title ?? this.title,
      price: price ?? this.price,
      description: description ?? this.description,
      isInCart: isInCart ?? this.isInCart,
      sellerId: sellerId ?? this.sellerId,
      picture: picture ?? this.picture
    );
  }

  factory ProductListDto.fromJson(Map<String, dynamic> json) {
    List<Map<String, dynamic>> pictureList = (json['pictures'] as List)
      .map((picture) => picture as Map<String, dynamic>)
      .toList();

    return ProductListDto(
      id: json['id'],
      title: json['title'],
      price: (json['price'] as num).toDouble(),
      description: json['description'],
      isInCart: json['isInCart'],
      sellerId: json['sellerId'],
      picture: pictureList.isEmpty ? null : PictureDto.fromJson(pictureList.first),
    );
  }

  factory ProductListDto.fromProductDto(ProductDto dto) {
    return ProductListDto(
      id: dto.id,
      title: dto.title,
      price: dto.price,
      description: dto.description,
      isInCart: dto.isInCart,
      sellerId: dto.seller.id,
      picture: dto.pictures.isEmpty ? null : dto.pictures.first,
    );
  }
}
