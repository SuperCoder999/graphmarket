import 'package:image_picker/image_picker.dart';
import 'package:marketplace/dtos/files/file.dart';

class UpdateProductDto {
  String id;
  String title;
  double price;
  String description;
  List<XFile> createdPictures;
  List<String> deletedPictures;

  UpdateProductDto({
    required this.id,
    required this.title,
    required this.price,
    required this.description,
    this.createdPictures = const [],
    this.deletedPictures = const [],
  });

  Future<Map<String, dynamic>> toJson() async => {
    'id': id,
    'title': title,
    'price': price,
    'description': description,
    'pictures': await Future.wait(createdPictures
      .map((picture) => FileDto.fromXFileToJson(picture))),
    'deletedPictures': deletedPictures,
  };
}
