import 'package:marketplace/dtos/product/product-list.dart';

class ProductPaginatedListDto {
  final bool hasMoreProducts;
  final List<ProductListDto> products;

  const ProductPaginatedListDto({
    required this.hasMoreProducts,
    required this.products,
  });

  factory ProductPaginatedListDto.fromJson(Map<String, dynamic> json) {
    return ProductPaginatedListDto(
      hasMoreProducts: json['hasMoreProducts'],
      products: (json['products'] as List)
        .map((product) => ProductListDto.fromJson(product))
        .toList(),
    );
  }
}
