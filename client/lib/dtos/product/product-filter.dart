import 'package:marketplace/helpers/clonable.dart';

class ProductFilterDto implements Clonable<ProductFilterDto> {
  final int page;
  final String search;

  const ProductFilterDto({
    required this.page,
    required this.search,
  });

  const ProductFilterDto.initial() :
    page = 0,
    search = '';

  Map<String, dynamic> toJson() => {
    'page': page.toString(),
    'search': search,
  };

  @override
  ProductFilterDto cloneWith({
    int? page,
    int? perPage,
    String? search,
  }) => ProductFilterDto(
    page: page ?? this.page,
    search: search ?? this.search,
  );
}
