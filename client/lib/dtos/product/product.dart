import 'package:marketplace/dtos/picture/picure.dart';
import 'package:marketplace/dtos/user/seller.dart';
import 'package:marketplace/helpers/clonable.dart';

class ProductDto implements Clonable<ProductDto> {
  String id;
  String title;
  double price;
  String description;
  List<PictureDto> pictures;
  SellerDto seller;
  bool isInCart;

  ProductDto({
    required this.id,
    required this.title,
    required this.price,
    required this.description,
    required this.pictures,
    required this.seller,
    required this.isInCart,
  });

  @override
  ProductDto cloneWith({
    String? id,
    String? title,
    double? price,
    String? description,
    List<PictureDto>? pictures,
    SellerDto? seller,
    bool? isInCart,
  }) {
    return ProductDto(
      id: id ?? this.id,
      title: title ?? this.title,
      price: price ?? this.price,
      description: description ?? this.description,
      pictures: pictures ?? this.pictures,
      seller: seller ?? this.seller,
      isInCart: isInCart ?? this.isInCart,
    );
  }

  factory ProductDto.fromJson(Map<String, dynamic> json) {
    return ProductDto(
      id: json['id'],
      title: json['title'],
      price: (json['price'] as num).toDouble(),
      description: json['description'],
      pictures: (json['pictures'] as List)
        .map((picture) => PictureDto.fromJson(picture))
        .toList(),
      seller: SellerDto.fromJson(json['seller']),
      isInCart: json['isInCart'],
    );
  }
}
