import 'package:image_picker/image_picker.dart';
import 'package:marketplace/dtos/files/file.dart';

class CreateProductDto {
  String title;
  double price;
  String description;
  List<XFile> pictures;

  CreateProductDto({
    required this.title,
    required this.price,
    required this.description,
    this.pictures = const [],
  });

  Future<Map<String, dynamic>> toJson() async => {
    'title': title,
    'price': price,
    'description': description,
    'pictures': await Future.wait(pictures
      .map((picture) => FileDto.fromXFileToJson(picture))),
  };
}
