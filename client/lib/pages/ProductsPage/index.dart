import 'package:flutter/material.dart';
import 'package:marketplace/containers/DefaultPageWrapper/index.dart';
import 'package:marketplace/containers/ProductsList/index.dart';

class ProductsPage extends StatelessWidget {
  const ProductsPage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const DefaultPageWrapper(
      title: 'Products',
      child: ProductsList(),
    );
  }
}
