import 'package:flutter/material.dart';
import 'package:marketplace/containers/DefaultPageWrapper/index.dart';
import 'package:marketplace/containers/Register/index.dart';

class RegisterPage extends StatelessWidget {
  const RegisterPage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const DefaultPageWrapper(
      title: 'Regsiter',
      child: Register(),
    );
  }
}
