import 'package:flutter/material.dart';
import 'package:marketplace/containers/DefaultPageWrapper/index.dart';
import 'package:marketplace/containers/Login/index.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const DefaultPageWrapper(
      title: 'Login',
      child: Login(),
    );
  }
}
