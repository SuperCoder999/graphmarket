import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:marketplace/containers/CreateProduct/index.dart';
import 'package:marketplace/containers/DefaultPageWrapper/index.dart';
import 'package:marketplace/containers/UpdateProduct/index.dart';
import 'package:marketplace/dtos/product/product.dart';
import 'package:marketplace/redux/state.dart';

class ModifyProductPage extends StatelessWidget {
  const ModifyProductPage({ Key? key }) :super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<MarketplaceState, ProductDto?>(
      converter: (store) => store.state.products.updatingProduct,
      builder: (context, updating) {
        if (updating != null) {
          return const DefaultPageWrapper(
            title: 'Edit product',
            child: UpdateProduct(),
          );
        }

        return const DefaultPageWrapper(
          title: 'Add product',
          child: CreateProduct(),
        );
      },
    );
  }
}
