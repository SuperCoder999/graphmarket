import 'package:flutter/material.dart';
import 'package:marketplace/containers/DefaultPageWrapper/index.dart';
import 'package:marketplace/containers/ExpandedProduct/index.dart';

class ExpandedProductPage extends StatelessWidget {
  const ExpandedProductPage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const DefaultPageWrapper(
      title: 'Details',
      child: ExpandedProduct(),
    );
  }
}
