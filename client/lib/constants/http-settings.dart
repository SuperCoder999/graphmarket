import 'package:flutter_dotenv/flutter_dotenv.dart';

class HttpSettings {
  static String apiBaseUrl = dotenv.env['API_URL'] ?? '';
  static const String attachmentDefaultFieldName = 'file';
}
