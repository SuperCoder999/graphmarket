import 'package:flutter/material.dart';

class Settings {
  static const String appName = 'Marketplace';
  static const int productsPerPage = 40;

  static final ThemeData theme = ThemeData.from(
    colorScheme: ColorScheme.fromSwatch(
      primarySwatch: Colors.blue,
      backgroundColor: Colors.white,
    ),
  );
}
