# Marketplace

## Installation

- Install and configure `Android Studio`, `Android SDK` and `Flutter SDK`
- In both `client` and `server` folders copy contents from `.env.example` file, paste into `.env` file, and replace with your own values. **Note:** `BASE_URL` is used for client, so remember to replace `localhost` with `10.0.2.2`
- There is a deployed version of this server with cloud database here: https://graphmarket-server.herokuapp.com/
- `cd client`
- `echo "sdk_dir=/path/to/your/Android/sdk" > ./android/local.properties`
- `flutter pub get`
- `cd ../server`
- `npm install`
- `prisma db push`

## Run

### Server

- `npm run dev`

### Client

#### IOS or Web

- Unsupported

#### Android

- `flutter emulator --launch <name_of_your_Android_emulator>` (once)
- `flutter run` (`r` to reload, `R` to hard reload)

## Notes

- For global state and global functions I've used redux + flutter_redux + redux_thunk
- I didn't use graphql_flutter, it's widgets and store because:
  1. In some places there are many function and state selectors, and it's easier with StoreConnector.
  2. To get something from graphql store we need variables, and in some places application's state structure is different from graphql.
- I make phone call using url_launcher (deep linking, `tel:` link)
- shared_preferences is an AsyncStorage
